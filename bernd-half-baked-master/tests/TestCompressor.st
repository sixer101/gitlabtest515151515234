{Test}
FUNCTION Compressor_Idle
    VAR_TEMP
        IsRunning : Bool;
        Error : Bool;
        HwOut : Bool;
        InternalValues : Lib.CompressorInternalVal;
    END_VAR

        InternalValues.CountEnable   := FALSE;
        InternalValues.CountValue    := 0;
        InternalValues.InternalState := 0;

        Lib.Compressor(
             ReqEnable      := FALSE
            ,StartDelay_ms  := 100
            ,CycleTime_ms   := 10
            ,IsRunning      => IsRunning
            ,Error          => Error
            ,HwOutCompressor=> HwOut
            ,InternalValues := InternalValues);

        AxUnit.Assert.Equal(FALSE, IsRunning);
        AxUnit.Assert.Equal(FALSE, Error);
        AxUnit.Assert.Equal(FALSE, HwOut);
END_FUNCTION

{Test}
FUNCTION Compressor_error_no_cycletime
    VAR_TEMP
        IsRunning : Bool;
        Error : Bool;
        HwOut : Bool;
        InternalValues : Lib.CompressorInternalVal;
    END_VAR

        InternalValues.CountEnable   := FALSE;
        InternalValues.CountValue    := 0;
        InternalValues.InternalState := 0;

        Lib.Compressor(
             ReqEnable      := TRUE
            ,StartDelay_ms  := 100
            ,CycleTime_ms   := 0
            ,IsRunning      => IsRunning
            ,Error          => Error
            ,HwOutCompressor=> HwOut
            ,InternalValues := InternalValues);

        AxUnit.Assert.Equal(FALSE, IsRunning);
        AxUnit.Assert.Equal(TRUE, Error);
        AxUnit.Assert.Equal(FALSE, HwOut);
END_FUNCTION

{Test}
FUNCTION Compressor_error_negative_cycletime
    VAR_TEMP
        IsRunning : Bool;
        Error : Bool;
        HwOut : Bool;
        InternalValues : Lib.CompressorInternalVal;
    END_VAR

        InternalValues.CountEnable   := FALSE;
        InternalValues.CountValue    := 0;
        InternalValues.InternalState := 0;

        Lib.Compressor(
             ReqEnable      := TRUE
            ,StartDelay_ms  := 100
            ,CycleTime_ms   := -1000
            ,IsRunning      => IsRunning
            ,Error          => Error
            ,HwOutCompressor=> HwOut
            ,InternalValues := InternalValues);

        AxUnit.Assert.Equal(FALSE, IsRunning);
        AxUnit.Assert.Equal(TRUE, Error);
        AxUnit.Assert.Equal(FALSE, HwOut);
END_FUNCTION

{Test}
FUNCTION Compressor_start
    VAR_TEMP
        IsRunning : Bool;
        Error : Bool;
        HwOut : Bool;
        InternalValues : Lib.CompressorInternalVal;
    END_VAR

        InternalValues.CountEnable   := FALSE;
        InternalValues.CountValue    := 0;
        InternalValues.InternalState := 0;

        Lib.Compressor(
             ReqEnable      := TRUE
            ,StartDelay_ms  := 100
            ,CycleTime_ms   := 10
            ,IsRunning      => IsRunning
            ,Error          => Error
            ,HwOutCompressor=> HwOut
            ,InternalValues := InternalValues);

        AxUnit.Assert.Equal(FALSE, IsRunning);
        AxUnit.Assert.Equal(FALSE, Error);
        AxUnit.Assert.Equal(TRUE, HwOut);
END_FUNCTION

{Test}
FUNCTION Compressor_wait_for_enable
    VAR_TEMP
        IsRunning : Bool;
        Error : Bool;
        HwOut : Bool;
        InternalValues : Lib.CompressorInternalVal;
    END_VAR

        InternalValues.CountEnable   := FALSE;
        InternalValues.CountValue    := 5;
        InternalValues.InternalState := 1;

        Lib.Compressor(
             ReqEnable      := FALSE
            ,StartDelay_ms  := 100
            ,CycleTime_ms   := 10
            ,IsRunning      => IsRunning
            ,Error          => Error
            ,HwOutCompressor=> HwOut
            ,InternalValues := InternalValues);

        AxUnit.Assert.Equal(FALSE, IsRunning);
        AxUnit.Assert.Equal(FALSE, Error);
        AxUnit.Assert.Equal(FALSE, HwOut);
END_FUNCTION

{Test}
FUNCTION Compressor_start_delay_ended
    VAR_TEMP
        IsRunning : Bool;
        Error : Bool;
        HwOut : Bool;
        InternalValues : Lib.CompressorInternalVal;
    END_VAR

        InternalValues.CountEnable   := TRUE;
        InternalValues.CountValue    := 11;
        InternalValues.InternalState := 1;

        Lib.Compressor(
             ReqEnable      := FALSE
            ,StartDelay_ms  := 100
            ,CycleTime_ms   := 10
            ,IsRunning      => IsRunning
            ,Error          => Error
            ,HwOutCompressor=> HwOut
            ,InternalValues := InternalValues);

        AxUnit.Assert.Equal(TRUE, IsRunning);
        AxUnit.Assert.Equal(FALSE, Error);
        AxUnit.Assert.Equal(TRUE, HwOut);
END_FUNCTION

{Test}
FUNCTION Compressor_continue
    VAR_TEMP
        IsRunning : Bool;
        Error : Bool;
        HwOut : Bool;
        InternalValues : Lib.CompressorInternalVal;
    END_VAR

        InternalValues.CountEnable   := FALSE;
        InternalValues.CountValue    := 0;
        InternalValues.InternalState := 2;

        Lib.Compressor(
             ReqEnable      := TRUE
            ,StartDelay_ms  := 100
            ,CycleTime_ms   := 10
            ,IsRunning      => IsRunning
            ,Error          => Error
            ,HwOutCompressor=> HwOut
            ,InternalValues := InternalValues);

        AxUnit.Assert.Equal(TRUE, IsRunning);
        AxUnit.Assert.Equal(FALSE, Error);
        AxUnit.Assert.Equal(TRUE, HwOut);
END_FUNCTION

{Test}
FUNCTION Compressor_disable
    VAR_TEMP
        IsRunning : Bool;
        Error : Bool;
        HwOut : Bool;
        InternalValues : Lib.CompressorInternalVal;
    END_VAR

        InternalValues.CountEnable   := FALSE;
        InternalValues.CountValue    := 0;
        InternalValues.InternalState := 2;

        Lib.Compressor(
             ReqEnable      := FALSE
            ,StartDelay_ms  := 100
            ,CycleTime_ms   := 10
            ,IsRunning      => IsRunning
            ,Error          => Error
            ,HwOutCompressor=> HwOut
            ,InternalValues := InternalValues);

        AxUnit.Assert.Equal(FALSE, IsRunning);
        AxUnit.Assert.Equal(FALSE, Error);
        AxUnit.Assert.Equal(FALSE, HwOut);
END_FUNCTION

{Test}
FUNCTION Compressor_reset_error
    VAR_TEMP
        IsRunning : Bool;
        Error : Bool;
        HwOut : Bool;
        InternalValues : Lib.CompressorInternalVal;
    END_VAR

        InternalValues.CountEnable   := FALSE;
        InternalValues.CountValue    := 0;
        InternalValues.InternalState := 10;

        Lib.Compressor(
             ReqEnable      := FALSE
            ,StartDelay_ms  := 100
            ,CycleTime_ms   := 10
            ,IsRunning      => IsRunning
            ,Error          => Error
            ,HwOutCompressor=> HwOut
            ,InternalValues := InternalValues);

        AxUnit.Assert.Equal(FALSE, IsRunning);
        AxUnit.Assert.Equal(FALSE, Error);
        AxUnit.Assert.Equal(FALSE, HwOut);
END_FUNCTION