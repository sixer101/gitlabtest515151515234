NAMESPACE AxUnit

NAMESPACE Assert

  FUNCTION Equal
            VAR_INPUT
		         expected:  INT;
		         actual:  INT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION Equal
            VAR_INPUT
		         expected:  SINT;
		         actual:  SINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION Equal
            VAR_INPUT
		         expected:  DINT;
		         actual:  DINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION Equal
            VAR_INPUT
		         expected:  LINT;
		         actual:  LINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION Equal
            VAR_INPUT
		         expected:  USINT;
		         actual:  USINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION Equal
            VAR_INPUT
		         expected:  UINT;
		         actual:  UINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION Equal
            VAR_INPUT
		         expected:  UDINT;
		         actual:  UDINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION Equal
            VAR_INPUT
		         expected:  ULINT;
		         actual:  ULINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION Equal
            VAR_INPUT
		         expected:  REAL;
		         actual:  REAL;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION Equal
            VAR_INPUT
		         expected:  LREAL;
		         actual:  LREAL;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION Equal
            VAR_INPUT
		         expected:  BOOL;
		         actual:  BOOL;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION Equal
            VAR_INPUT
		         expected:  BYTE;
		         actual:  BYTE;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION Equal
            VAR_INPUT
		         expected:  WORD;
		         actual:  WORD;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION Equal
            VAR_INPUT
		         expected:  DWORD;
		         actual:  DWORD;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION Equal
            VAR_INPUT
		         expected:  LWORD;
		         actual:  LWORD;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION NotEqual
            VAR_INPUT
		         expected:  INT;
		         actual:  INT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION NotEqual
            VAR_INPUT
		         expected:  SINT;
		         actual:  SINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION NotEqual
            VAR_INPUT
		         expected:  DINT;
		         actual:  DINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION NotEqual
            VAR_INPUT
		         expected:  LINT;
		         actual:  LINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION NotEqual
            VAR_INPUT
		         expected:  USINT;
		         actual:  USINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION NotEqual
            VAR_INPUT
		         expected:  UINT;
		         actual:  UINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION NotEqual
            VAR_INPUT
		         expected:  UDINT;
		         actual:  UDINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION NotEqual
            VAR_INPUT
		         expected:  ULINT;
		         actual:  ULINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION NotEqual
            VAR_INPUT
		         expected:  REAL;
		         actual:  REAL;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION NotEqual
            VAR_INPUT
		         expected:  LREAL;
		         actual:  LREAL;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION NotEqual
            VAR_INPUT
		         expected:  BOOL;
		         actual:  BOOL;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION NotEqual
            VAR_INPUT
		         expected:  BYTE;
		         actual:  BYTE;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION NotEqual
            VAR_INPUT
		         expected:  WORD;
		         actual:  WORD;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION NotEqual
            VAR_INPUT
		         expected:  DWORD;
		         actual:  DWORD;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION NotEqual
            VAR_INPUT
		         expected:  LWORD;
		         actual:  LWORD;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION GreaterThan
            VAR_INPUT
		         left:  INT;
		         right:  INT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION GreaterThan
            VAR_INPUT
		         left:  SINT;
		         right:  SINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION GreaterThan
            VAR_INPUT
		         left:  DINT;
		         right:  DINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION GreaterThan
            VAR_INPUT
		         left:  LINT;
		         right:  LINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION GreaterThan
            VAR_INPUT
		         left:  USINT;
		         right:  USINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION GreaterThan
            VAR_INPUT
		         left:  UINT;
		         right:  UINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION GreaterThan
            VAR_INPUT
		         left:  UDINT;
		         right:  UDINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION GreaterThan
            VAR_INPUT
		         left:  ULINT;
		         right:  ULINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION GreaterThan
            VAR_INPUT
		         left:  REAL;
		         right:  REAL;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION GreaterThan
            VAR_INPUT
		         left:  LREAL;
		         right:  LREAL;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION LessThan
            VAR_INPUT
		         left:  INT;
		         right:  INT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION LessThan
            VAR_INPUT
		         left:  SINT;
		         right:  SINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION LessThan
            VAR_INPUT
		         left:  DINT;
		         right:  DINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION LessThan
            VAR_INPUT
		         left:  LINT;
		         right:  LINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION LessThan
            VAR_INPUT
		         left:  USINT;
		         right:  USINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION LessThan
            VAR_INPUT
		         left:  UINT;
		         right:  UINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION LessThan
            VAR_INPUT
		         left:  UDINT;
		         right:  UDINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION LessThan
            VAR_INPUT
		         left:  ULINT;
		         right:  ULINT;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION LessThan
            VAR_INPUT
		         left:  REAL;
		         right:  REAL;
	        END_VAR
            ;
    END_FUNCTION 

  FUNCTION LessThan
            VAR_INPUT
		         left:  LREAL;
		         right:  LREAL;
	        END_VAR
            ;
    END_FUNCTION 

END_NAMESPACE

END_NAMESPACE

