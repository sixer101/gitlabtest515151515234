{Test(val1 := 0, val2 := 0)}
{Test(val1 := 0, val2 := 1)}
{Test(val1 := 1, val2 := 0)}
{Test(val1 := 1, val2 := 1)}
FUNCTION cCompressor_Test_mInit
    VAR_INPUT
        val1 : Int;
        val2 : Int;
    END_VAR
    VAR_TEMP
        CmprInst : Lib.cCompressor;
        Result : Bool;
    END_VAR

        Result := CmprInst.mInit(SwitchOnTime_ms := val1, CycleTime_ms := val2);
        
        AxUnit.Assert.Equal(expected := (val1 > 0 AND val2 > 0), actual := Result);
END_FUNCTION

{Test(val1 := 0, val2 := 0)}
{Test(val1 := 0, val2 := 1)}
{Test(val1 := 1, val2 := 0)}
{Test(val1 := 1, val2 := 1)}
FUNCTION cCompressor_Test_mSwitchOn
    VAR_INPUT
        val1 : Int;
        val2 : Int;
    END_VAR
    VAR_TEMP
        CmprInst : Lib.cCompressor;
        Result : Bool;
    END_VAR
        //init the cCompressor
        Result := CmprInst.mInit(SwitchOnTime_ms := val1, CycleTime_ms := val2);

        //try to switch on
        Result := CmprInst.mSwitchOn();
        
        AxUnit.Assert.Equal(expected := (val1 > 0 AND val2 > 0), actual := Result);
END_FUNCTION

{Test}
FUNCTION cCompressor_Test_setOutput_when_switching_on
    VAR_TEMP
        CmprInst : Lib.cCompressor;
        Result : Bool;
    END_VAR
        //init the cCompressor
        Result := CmprInst.mInit(SwitchOnTime_ms := 1, CycleTime_ms := 1);
        //try to switch on
        Result := CmprInst.mSwitchOn();
        //call cyclic method 
        CmprInst.mMainCyclic(HwOutCompressor => Result);
        CmprInst.mMainCyclic(HwOutCompressor => Result);
        
        AxUnit.Assert.Equal(expected := TRUE, actual := Result);
END_FUNCTION