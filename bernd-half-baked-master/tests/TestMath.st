{Test(summand1 := 1, summand2 := 2, sum := 3)}
{Test(summand1 := 0, summand2 := 0, sum := 0)}
{Test(summand1 := 1000, summand2 := 1, sum := 1001)}
FUNCTION TestAddition
    VAR_INPUT
        summand1 :Int;
        summand2 :Int;
        sum :Int;
    END_VAR
    VAR_TEMP
    END_VAR


    AxUnit.Assert.Equal(Math.Addition(summand1, summand2), sum);

END_FUNCTION


{Test(mul1 := 1, mul2 := 2, product := 2)}
{Test(mul1 := 2, mul2 := 3, product := 6)}
{Test(mul1 := 4, mul2 := 3, product := 12)}
FUNCTION TestMul
    VAR_INPUT
        mul1 :Int;
        mul2 :Int;
        product :Int;
    END_VAR
    VAR_TEMP
    END_VAR

    AxUnit.Assert.Equal(Math.Multiplication(mul1, mul2), product);
        ;        
END_FUNCTION

