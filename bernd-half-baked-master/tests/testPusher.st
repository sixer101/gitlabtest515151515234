{Test}
FUNCTION Pusher_Idle
VAR_TEMP
    Busy : Bool;
    Error : Bool;
    Done : Bool;
    HwOutPusher : Bool;
    ReqEnCompressor : Bool;
    PusherInternalValues : Lib.PusherInternalVal;
END_VAR

PusherInternalValues.CountEnable    := FALSE;
PusherInternalValues.CountValue     := 0;
PusherInternalValues.InternalState  := 0;

Lib.Pusher(
     ExecutePush            := FALSE
    ,CompressorIsRunning    := FALSE
    ,PushTime_ms            := 0
    ,CompressorTimeout_ms   := 0
    ,CallCycleTime_ms       := 0
    ,Busy               => Busy
    ,Error              => Error
    ,Done               => Done
    ,HwOutPusher        => HwOutPusher
    ,ReqEnCompressor    => ReqEnCompressor
    ,InternalValues     := PusherInternalValues
    );

    AxUnit.Assert.Equal(FALSE, Busy);
    AxUnit.Assert.Equal(FALSE, Error);
    AxUnit.Assert.Equal(FALSE, Done);
    AxUnit.Assert.Equal(0, PusherInternalValues.InternalState);
    
END_FUNCTION

{Test}
FUNCTION Pusher_wrong_CycleTime
VAR_TEMP
    Busy : Bool;
    Error : Bool;
    Done : Bool;
    HwOutPusher : Bool;
    ReqEnCompressor : Bool;
    PusherInternalValues : Lib.PusherInternalVal;
END_VAR

PusherInternalValues.CountEnable    := FALSE;
PusherInternalValues.CountValue     := 0;
PusherInternalValues.InternalState  := 0;

Lib.Pusher(
     ExecutePush            := TRUE
    ,CompressorIsRunning    := FALSE
    ,PushTime_ms            := 200
    ,CompressorTimeout_ms   := 200
    ,CallCycleTime_ms       := 0
    ,Busy               => Busy
    ,Error              => Error
    ,Done               => Done
    ,HwOutPusher        => HwOutPusher
    ,ReqEnCompressor    => ReqEnCompressor
    ,InternalValues     := PusherInternalValues
    );

    AxUnit.Assert.Equal(FALSE, Busy);
    AxUnit.Assert.Equal(TRUE, Error);
    AxUnit.Assert.Equal(FALSE, Done);
END_FUNCTION

{Test}
FUNCTION Pusher_wrong_CompressorTimeout
VAR_TEMP
    Busy : Bool;
    Error : Bool;
    Done : Bool;
    HwOutPusher : Bool;
    ReqEnCompressor : Bool;
    PusherInternalValues : Lib.PusherInternalVal;
END_VAR

PusherInternalValues.CountEnable    := FALSE;
PusherInternalValues.CountValue     := 0;
PusherInternalValues.InternalState  := 0;

Lib.Pusher(
     ExecutePush            := TRUE
    ,CompressorIsRunning    := FALSE
    ,PushTime_ms            := 200
    ,CompressorTimeout_ms   := 0
    ,CallCycleTime_ms       := 10
    ,Busy               => Busy
    ,Error              => Error
    ,Done               => Done
    ,HwOutPusher        => HwOutPusher
    ,ReqEnCompressor    => ReqEnCompressor
    ,InternalValues     := PusherInternalValues
    );

    AxUnit.Assert.Equal(FALSE, Busy);
    AxUnit.Assert.Equal(TRUE, Error);
    AxUnit.Assert.Equal(FALSE, Done);
END_FUNCTION

{Test}
FUNCTION Pusher_wrong_PushTime
VAR_TEMP
    Busy : Bool;
    Error : Bool;
    Done : Bool;
    HwOutPusher : Bool;
    ReqEnCompressor : Bool;
    PusherInternalValues : Lib.PusherInternalVal;
END_VAR

PusherInternalValues.CountEnable    := FALSE;
PusherInternalValues.CountValue     := 0;
PusherInternalValues.InternalState  := 0;

Lib.Pusher(
     ExecutePush            := TRUE
    ,CompressorIsRunning    := FALSE
    ,PushTime_ms            := 0
    ,CompressorTimeout_ms   := 200
    ,CallCycleTime_ms       := 10
    ,Busy               => Busy
    ,Error              => Error
    ,Done               => Done
    ,HwOutPusher        => HwOutPusher
    ,ReqEnCompressor    => ReqEnCompressor
    ,InternalValues     := PusherInternalValues
    );

    AxUnit.Assert.Equal(FALSE, Busy);
    AxUnit.Assert.Equal(TRUE, Error);
    AxUnit.Assert.Equal(FALSE, Done); 
END_FUNCTION

{Test}
FUNCTION Pusher_req_enable_compressor
VAR_TEMP
    Busy : Bool;
    Error : Bool;
    Done : Bool;
    HwOutPusher : Bool;
    ReqEnCompressor : Bool;
    PusherInternalValues : Lib.PusherInternalVal;
END_VAR

PusherInternalValues.CountEnable    := FALSE;
PusherInternalValues.CountValue     := 0;
PusherInternalValues.InternalState  := 0;

Lib.Pusher(
     ExecutePush            := TRUE
    ,CompressorIsRunning    := FALSE
    ,PushTime_ms            := 200
    ,CompressorTimeout_ms   := 200
    ,CallCycleTime_ms       := 10
    ,Busy               => Busy
    ,Error              => Error
    ,Done               => Done
    ,HwOutPusher        => HwOutPusher
    ,ReqEnCompressor    => ReqEnCompressor
    ,InternalValues     := PusherInternalValues
    );

    AxUnit.Assert.Equal(TRUE, Busy);
    AxUnit.Assert.Equal(FALSE, Error);
    AxUnit.Assert.Equal(FALSE, Done); 
    AxUnit.Assert.Equal(TRUE, ReqEnCompressor);
END_FUNCTION

{Test}
FUNCTION Pusher_timeout_compressor
VAR_TEMP
    Busy : Bool;
    Error : Bool;
    Done : Bool;
    HwOutPusher : Bool;
    ReqEnCompressor : Bool;
    PusherInternalValues : Lib.PusherInternalVal;
END_VAR

PusherInternalValues.CountEnable    := TRUE;
PusherInternalValues.CountValue     := 21;
PusherInternalValues.InternalState  := 10; //defined: Step 10 is waiting for compressor to turn on

Lib.Pusher(
     ExecutePush            := TRUE
    ,CompressorIsRunning    := FALSE
    ,PushTime_ms            := 200
    ,CompressorTimeout_ms   := 200
    ,CallCycleTime_ms       := 10
    ,Busy               => Busy
    ,Error              => Error
    ,Done               => Done
    ,HwOutPusher        => HwOutPusher
    ,ReqEnCompressor    => ReqEnCompressor
    ,InternalValues     := PusherInternalValues
    );

    AxUnit.Assert.Equal(FALSE, Busy);
    AxUnit.Assert.Equal(TRUE, Error);
    AxUnit.Assert.Equal(FALSE, Done); 
    AxUnit.Assert.Equal(FALSE, HwOutPusher);
END_FUNCTION

{Test}
FUNCTION Pusher_Push
VAR_TEMP
    Busy : Bool;
    Error : Bool;
    Done : Bool;
    HwOutPusher : Bool;
    ReqEnCompressor : Bool;
    PusherInternalValues : Lib.PusherInternalVal;
END_VAR

PusherInternalValues.CountEnable    := TRUE;
PusherInternalValues.CountValue     := 10; //waited 100ms to turn on compressor
PusherInternalValues.InternalState  := 10; //in step waiting for compressor feedback

Lib.Pusher(
     ExecutePush            := TRUE
    ,CompressorIsRunning    := TRUE
    ,PushTime_ms            := 200
    ,CompressorTimeout_ms   := 200
    ,CallCycleTime_ms       := 10
    ,Busy               => Busy
    ,Error              => Error
    ,Done               => Done
    ,HwOutPusher        => HwOutPusher
    ,ReqEnCompressor    => ReqEnCompressor
    ,InternalValues     := PusherInternalValues
    );

    AxUnit.Assert.Equal(TRUE, Busy);
    AxUnit.Assert.Equal(FALSE, Error);
    AxUnit.Assert.Equal(FALSE, Done); 
    AxUnit.Assert.Equal(TRUE, HwOutPusher);
END_FUNCTION

{Test}
FUNCTION Pusher_Push_continue
VAR_TEMP
    Busy : Bool;
    Error : Bool;
    Done : Bool;
    HwOutPusher : Bool;
    ReqEnCompressor : Bool;
    PusherInternalValues : Lib.PusherInternalVal;
END_VAR

PusherInternalValues.CountEnable    := TRUE;
PusherInternalValues.CountValue     := 10; //already pushed 100ms
PusherInternalValues.InternalState  := 20; //in step pushing

Lib.Pusher(
     ExecutePush            := TRUE
    ,CompressorIsRunning    := TRUE
    ,PushTime_ms            := 200
    ,CompressorTimeout_ms   := 200
    ,CallCycleTime_ms       := 10
    ,Busy               => Busy
    ,Error              => Error
    ,Done               => Done
    ,HwOutPusher        => HwOutPusher
    ,ReqEnCompressor    => ReqEnCompressor
    ,InternalValues     := PusherInternalValues
    );

    AxUnit.Assert.Equal(TRUE, Busy);
    AxUnit.Assert.Equal(FALSE, Error);
    AxUnit.Assert.Equal(FALSE, Done); 
    AxUnit.Assert.Equal(TRUE, HwOutPusher);
END_FUNCTION

{Test}
FUNCTION Pusher_Push_end
VAR_TEMP
    Busy : Bool;
    Error : Bool;
    Done : Bool;
    HwOutPusher : Bool;
    ReqEnCompressor : Bool;
    PusherInternalValues : Lib.PusherInternalVal;
END_VAR

PusherInternalValues.CountEnable    := TRUE;
PusherInternalValues.CountValue     := 21; //pushed long enough
PusherInternalValues.InternalState  := 20; //in step pushing

Lib.Pusher(
     ExecutePush            := TRUE
    ,CompressorIsRunning    := TRUE
    ,PushTime_ms            := 200
    ,CompressorTimeout_ms   := 200
    ,CallCycleTime_ms       := 10
    ,Busy               => Busy
    ,Error              => Error
    ,Done               => Done
    ,HwOutPusher        => HwOutPusher
    ,ReqEnCompressor    => ReqEnCompressor
    ,InternalValues     := PusherInternalValues
    );

    AxUnit.Assert.Equal(FALSE, Busy);
    AxUnit.Assert.Equal(FALSE, Error);
    AxUnit.Assert.Equal(TRUE, Done);
    AxUnit.Assert.Equal(FALSE, HwOutPusher);
    AxUnit.Assert.Equal(FALSE, ReqEnCompressor);
    AxUnit.Assert.Equal(FALSE, PusherInternalValues.CountEnable);
END_FUNCTION

{Test}
FUNCTION Pusher_Push_done
VAR_TEMP
    Busy : Bool;
    Error : Bool;
    Done : Bool;
    HwOutPusher : Bool;
    ReqEnCompressor : Bool;
    PusherInternalValues : Lib.PusherInternalVal;
END_VAR

PusherInternalValues.CountEnable    := FALSE;
PusherInternalValues.CountValue     := 0; //pushed long enough
PusherInternalValues.InternalState  := 30; //in step done

Lib.Pusher(
     ExecutePush            := TRUE
    ,CompressorIsRunning    := FALSE
    ,PushTime_ms            := 200
    ,CompressorTimeout_ms   := 200
    ,CallCycleTime_ms       := 10
    ,Busy               => Busy
    ,Error              => Error
    ,Done               => Done
    ,HwOutPusher        => HwOutPusher
    ,ReqEnCompressor    => ReqEnCompressor
    ,InternalValues     := PusherInternalValues
    );

    AxUnit.Assert.Equal(FALSE, Busy);
    AxUnit.Assert.Equal(FALSE, Error);
    AxUnit.Assert.Equal(TRUE, Done);
    AxUnit.Assert.Equal(FALSE, HwOutPusher);
    AxUnit.Assert.Equal(FALSE, ReqEnCompressor);
END_FUNCTION

{Test}
FUNCTION Pusher_reset_done
VAR_TEMP
    Busy : Bool;
    Error : Bool;
    Done : Bool;
    HwOutPusher : Bool;
    ReqEnCompressor : Bool;
    PusherInternalValues : Lib.PusherInternalVal;
END_VAR

PusherInternalValues.CountEnable    := FALSE;
PusherInternalValues.CountValue     := 0;
PusherInternalValues.InternalState  := 30; //in step pushing done

Lib.Pusher(
     ExecutePush            := FALSE
    ,CompressorIsRunning    := TRUE
    ,PushTime_ms            := 200
    ,CompressorTimeout_ms   := 200
    ,CallCycleTime_ms       := 10
    ,Busy               => Busy
    ,Error              => Error
    ,Done               => Done
    ,HwOutPusher        => HwOutPusher
    ,ReqEnCompressor    => ReqEnCompressor
    ,InternalValues     := PusherInternalValues
    );

    AxUnit.Assert.Equal(FALSE, Busy);
    AxUnit.Assert.Equal(FALSE, Error);
    AxUnit.Assert.Equal(FALSE, Done);    
    AxUnit.Assert.Equal(FALSE, HwOutPusher);
END_FUNCTION

{Test}
FUNCTION Pusher_error_reset
VAR_TEMP
    Busy : Bool;
    Error : Bool;
    Done : Bool;
    HwOutPusher : Bool;
    ReqEnCompressor : Bool;
    PusherInternalValues : Lib.PusherInternalVal;
END_VAR

PusherInternalValues.CountEnable    := FALSE;
PusherInternalValues.CountValue     := 0;
PusherInternalValues.InternalState  := 40; //in step error

Lib.Pusher(
     ExecutePush            := FALSE
    ,CompressorIsRunning    := FALSE
    ,PushTime_ms            := 200
    ,CompressorTimeout_ms   := 200
    ,CallCycleTime_ms       := 10
    ,Busy               => Busy
    ,Error              => Error
    ,Done               => Done
    ,HwOutPusher        => HwOutPusher
    ,ReqEnCompressor    => ReqEnCompressor
    ,InternalValues     := PusherInternalValues
    );

    AxUnit.Assert.Equal(FALSE, Busy);
    AxUnit.Assert.Equal(FALSE, Error);
    AxUnit.Assert.Equal(FALSE, Done);    
    AxUnit.Assert.Equal(FALSE, HwOutPusher);
END_FUNCTION