{Test(Row := 1, Column := 1)}
{Test(Row := 0, Column := 0)}
{Test(Row := 5, Column := 1)}
{Test(Row := 1, Column := 5)}
{Test(Row := 1, Column := -1)}
{Test(Row := -1, Column := 1)}
FUNCTION cWarehouseShelf_Test_WriteItem
    //try to write a valid item type into some random position that may or not be existent
    VAR_INPUT
        Row : Int;
        Column : Int;
    END_VAR
    VAR_TEMP
        InstShelf : Lib.WH.cWarehouseShelf;
        Item : Lib.WH.WarehouseElement;
        Result : Bool;
    END_VAR
    Item.State := USINT#3; //filled with item
    Item.Colour := USINT#1; //white
    
    Result := InstShelf.mWriteItem(Row, Column, Item);
    AxUnit.Assert.Equal(expected := Row >= 1 AND Row <=3 AND Column >=1 AND Column <= 3, actual := Result);
END_FUNCTION

{Test}
FUNCTION cWarehouseShelf_Test_Search_1
    VAR_TEMP
        InstShelf : Lib.WH.cWarehouseShelf;
        Item : Lib.WH.WarehouseElement;
        Result : Bool;
        FoundPlace : Lib.WH.WarehouseElementPos;
    END_VAR
    //fill the warehouse
    //full, valid colour
    Item.State  := USINT#3;
    Item.Colour := USINT#1;
    Result      := InstShelf.mWriteItem(2, 1, Item);

    FoundPlace := InstShelf.mSearchInsideShelf(Element := Item); 
    AxUnit.Assert.Equal(expected := 2, actual := FoundPlace.Row);
    AxUnit.Assert.Equal(expected := 1, actual := FoundPlace.Column);
END_FUNCTION

{Test}
FUNCTION cWarehouseShelf_Test_Search_2
    VAR_TEMP
        InstShelf : Lib.WH.cWarehouseShelf;
        Item : Lib.WH.WarehouseElement;
        Result : Bool;
        FoundPlace : Lib.WH.WarehouseElementPos;
    END_VAR
    //fill the warehouse
    //empty, valid colour
    Item.State  := USINT#1; 
    Item.Colour := USINT#1; 
    Result      := InstShelf.mWriteItem(3, 1, Item);

    FoundPlace := InstShelf.mSearchInsideShelf(Element := Item); 
    AxUnit.Assert.Equal(expected := 3, actual := FoundPlace.Row);
    AxUnit.Assert.Equal(expected := 1, actual := FoundPlace.Column);
END_FUNCTION

{Test}
FUNCTION cWarehouseShelf_Test_Search_3
    VAR_TEMP
        InstShelf : Lib.WH.cWarehouseShelf;
        Item : Lib.WH.WarehouseElement;
        Result : Bool;
        FoundPlace : Lib.WH.WarehouseElementPos;
    END_VAR
    //fill the warehouse
    //empty, invalid colour
    Item.State  := USINT#1;
    Item.Colour := USINT#7;
    Result      := InstShelf.mWriteItem(1, 2, Item);

    FoundPlace := InstShelf.mSearchInsideShelf(Element := Item);  
    AxUnit.Assert.Equal(expected := 1, actual := FoundPlace.Row);
    AxUnit.Assert.Equal(expected := 2, actual := FoundPlace.Column);
END_FUNCTION

{Test}
FUNCTION cWarehouseShelf_Test_Search_4
    VAR_TEMP
        InstShelf : Lib.WH.cWarehouseShelf;
        Item : Lib.WH.WarehouseElement;
        Result : Bool;
        FoundPlace : Lib.WH.WarehouseElementPos;
    END_VAR
    //fill the warehouse
    //tray, valid colour
    Item.State  := USINT#2;
    Item.Colour := USINT#2;
    Result      := InstShelf.mWriteItem(1, 3, Item);

    FoundPlace := InstShelf.mSearchInsideShelf(Element := Item); 
    AxUnit.Assert.Equal(expected := 1, actual := FoundPlace.Row);
    AxUnit.Assert.Equal(expected := 3, actual := FoundPlace.Column);
END_FUNCTION

{Test}
FUNCTION cWarehouseShelf_Test_Search_5
    VAR_TEMP
        InstShelf : Lib.WH.cWarehouseShelf;
        Item : Lib.WH.WarehouseElement;
        Result : Bool;
        FoundPlace : Lib.WH.WarehouseElementPos;
    END_VAR
    //fill the warehouse
    //tray, invalid colour
    Item.State  := USINT#2;
    Item.Colour := USINT#0;
    Result      := InstShelf.mWriteItem(2, 3, Item);

    FoundPlace := InstShelf.mSearchInsideShelf(Element := Item); 
    AxUnit.Assert.Equal(expected := 2, actual := FoundPlace.Row);
    AxUnit.Assert.Equal(expected := 3, actual := FoundPlace.Column);
END_FUNCTION

{Test}
FUNCTION cWarehouseShelf_Test_Search_6_unsuccessful
    VAR_TEMP
        InstShelf : Lib.WH.cWarehouseShelf;
        FoundPlace : Lib.WH.WarehouseElementPos;
    END_VAR

    AxUnit.Assert.Equal(expected := 0, actual := FoundPlace.Row);
    AxUnit.Assert.Equal(expected := 0, actual := FoundPlace.Column);
END_FUNCTION