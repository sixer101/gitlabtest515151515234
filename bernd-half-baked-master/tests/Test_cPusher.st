{Test(val1 := 0, val2 := 0)}
{Test(val1 := 0, val2 := 1)}
{Test(val1 := 1, val2 := 0)}
{Test(val1 := 1, val2 := 1)}
FUNCTION cPusher_Test_mInit
    VAR_INPUT
        val1 : Int;
        val2 : Int;
    END_VAR
    VAR_TEMP
        Pusher : Lib.cPusher;
        Result : Bool;
    END_VAR

        Result := Pusher.mInit(ExtendTime_ms := val1, CycleTime_ms := val2);        
        AxUnit.Assert.Equal(expected := (val1 > 0 AND val2 > 0), actual := Result);
END_FUNCTION

{Test(val1 := 0, val2 := 0)}
{Test(val1 := 0, val2 := 1)}
{Test(val1 := 1, val2 := 0)}
{Test(val1 := 1, val2 := 1)}
FUNCTION cPusher_Test_Push
    VAR_INPUT
        val1 : Int;
        val2 : Int;
    END_VAR
    VAR_TEMP
        Pusher : Lib.cPusher;
        Compr : Lib.cCompressor;
        Result : Bool;
        DummyBool : Bool;
    END_VAR
        //Init pusher
        Result := Pusher.mInit(ExtendTime_ms := val1, CycleTime_ms := val2);
        //init compressor and switch it on
        EnableCompressorForTests(Compr := Compr);
        //try to push with valid/invalid config
        Result := Pusher.mPush(CompressorInstance := Compr);
        AxUnit.Assert.Equal(expected := (val1 > 0 AND val2 > 0), actual := Result);
END_FUNCTION

FUNCTION EnableCompressorForTests
    VAR_IN_OUT
        Compr : Lib.cCompressor;
    END_VAR
    VAR_TEMP
        RetVal : Bool;
    END_VAR
    RetVal := Compr.mInit(1, 1);
    RetVal := Compr.mSwitchOn();
    Compr.mMainCyclic();
    Compr.mMainCyclic();
END_FUNCTION