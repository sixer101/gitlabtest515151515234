# Bernd half baked

Bernd is AX internal automation product using latest features of our SDK. Especially `ST compiler`, `Downloader` and `AxUnit` as testing framework.

## Continuous Intgration

Continuous Intgration (CI) means that all the time new source code has been written and commited to the repository, it will be integrated in a product.
The CI concept relies on the build pipeline which validates the code in several steps 
* build the code
* test the code

### Build Artifacts

At the end of CI there are build results which can be used outside from the repository and the build pipeline. 
The Bernds pipeline creates 
* Test report showing test results and coverage of tested code
* Loadable binaries which can be downloaded to the Bernds PLC