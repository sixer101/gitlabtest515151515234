CONFIGURATION Bernd

    TASK Cycle      (Priority := 5, Interval := T#10ms);
    //Comment made from Appsec16 account
    //TASK Background (Priority := 1);

    PROGRAM P1 WITH Cycle : GeneralTasks;
    PROGRAM P2 WITH Cycle : Sorting;
    PROGRAM P3 WITH Cycle : Warehouse;
    PROGRAM P4 WITH Cycle : Gripper;
    PROGRAM P5 WITH Cycle : Oven;
    

    VAR_GLOBAL //global data for exchange between programs
        gboWareHouseItemReadyForPickup      : BOOL;
        gboWareHouseItemReadyForDrop        : BOOL;
        GlobalConfig                        : GlobalConfigDataType;

        //imitate global constants
        ColourInvalid                       : USINT := USINT#0;
        ColourUndefined                     : USINT := USINT#1;
        ColourWhite                         : USINT := USINT#2;
        ColourRed                           : USINT := USINT#3;
        ColourBlue                          : USINT := USINT#4;
    END_VAR

    VAR_GLOBAL //Warehouse
        //Input
        WhRefSwitchHorAxis                  AT %I0.0 : BOOL;
        WhLightBarrierInside                AT %I0.1 : BOOL;
        WhLightBarrierOutside               AT %I0.2 : BOOL;
        WhRefSwitchVertAxis                 AT %I0.3 : BOOL;
        WhTrailSensor1Lower                 AT %I0.4 : BOOL;
        WhTrailSensor2Upper                 AT %I0.5 : BOOL;
        WhRefSwitchCantileverFront          AT %I0.6 : BOOL;
        WhRefSwitchCantileverBack           AT %I0.7 : BOOL;
        //Output
        WhMotorConvBeltForw                 AT %Q0.0 : BOOL;
        WhMotorConvBeltBackw                AT %Q0.1 : BOOL;
        WhMotorHorToRack                    AT %Q0.2 : BOOL;
        WhMotorHorToConvBelt                AT %Q0.3 : BOOL;
        WhMotorVertDown                     AT %Q0.4 : BOOL;
        WhMotorVertUp                       AT %Q0.5 : BOOL;
        WhMotorCantileverForw               AT %Q0.6 : BOOL;
        WhMotorCantileverBackw              AT %Q0.7 : BOOL;
        //Counter module horizontal axis
        WhMotorHorAxisCountValue            AT %ID2  : DINT;  
        WhMotorHorAxisStatus                AT %ID14 : DINT;  
        WhMotorHorAxisLoadValue             AT %QD2  : DINT;  
        WhMotorHorAxisControlValue          AT %QW11 : UINT;  
        WhMotorHorAxisControlByte           AT %QB10 : USINT;
        //counter module vertical axis
        WhMotorVertAxisCountValue           AT %ID18 : DINT;
        WhMotorVertAxisStatus               AT %ID30 : DINT;
        WhMotorVertAxisLoadValue            AT %QD18 : DINT;
        WhMotorVertAxisControlValue         AT %QW27 : UINT;
        WhMotorVertAxisControlByte          AT %QB26 : USINT;

        DriveHorizontalRetVal               : INT;
        DriveHorizontalExecute              : BOOL;
        DriveHorizontalJog                  : BOOL;
        DriveHorizontalCommand              : INT;
        DriveHorizontalPosition             : DINT;
        DriveHorizontalDirection            : INT;

        DriveHorizontalBusy                 : BOOL;
        DriveHorizontalDone                 : BOOL;
        DriveHorizontalActiveCommand        : INT;

        DriveHorizontalInternalState        : INT;
        DriveHorizontalInternalCommand      : INT;
        DriveHorizontalInternalDirection    : INT;
        DriveHorizontalInternalPosition     : DINT;
        DriveHorizontalInternalEdgeDetect   : BOOL;
        DriveHorizontalInternalHomingState  : BOOL;

        DriveVerticalRetVal                 : INT;
        DriveVerticalExecute                : BOOL;
        DriveVerticalJog                    : BOOL;
        DriveVerticalCommand                : INT;
        DriveVerticalPosition               : DINT;
        DriveVerticalDirection              : INT;

        DriveVerticalBusy                   : BOOL;
        DriveVerticalDone                   : BOOL;
        DriveVerticalActiveCommand          : INT;

        DriveVerticalInternalState          : INT;
        DriveVerticalInternalCommand        : INT;
        DriveVerticalInternalDirection      : INT;
        DriveVerticalInternalPosition       : DINT;
        DriveVerticalInternalEdgeDetect     : BOOL;
        DriveVerticalInternalHomingState    : BOOL;

        WhStorage                           : Lib.WH.cWarehouseShelf;
        WhstatBayConfSet                    : BOOL;
        WhstatLoopNo                        : INT;
        WhstatStep                          : INT;
        WhstatState                         : INT;
        WhstatDrivesHomed                   : BOOL;
        WhstatPosVertical                   : DINT;
        WhstatPosHorizontal                 : DINT;
        WhstatSequencePosVertical           : DINT;
        WhstatSequencePosHorizontal         : DINT;
        WhstatCounterEnable                 : BOOL;
        WhstatCounterValue                  : INT;

        //warehouse positions
        WhPosBay1                           : WarehousePos;
        WhPosBayOffset                      : WarehousePos;
        WhPosPark                           : WarehousePos;
        WhPosTransfer                       : WarehousePos;
        WhPosDropOffset                     : WarehousePos;
    END_VAR

    VAR_GLOBAL //Oven
        //Input
        OvRefSwTurnTablePosVacuum           AT %I100.0 : BOOL;
        OvRefSwTurnTablePosBelt             AT %I100.1 : BOOL;
        OvLightBarrierEndOfConvBelt         AT %I100.2 : BOOL;
        OvRefSwTurnTablePosSaw              AT %I100.3 : BOOL;
        OvRefSwVacuumPosTurnTable           AT %I100.4 : BOOL;
        OvRefSwOvenFeederInside             AT %I100.5 : BOOL;
        OvRefSwOvenFeederOutside            AT %I100.6 : BOOL;
        OvRefSwVacuumPosOven                AT %I100.7 : BOOL;
        OvLightBarrierOven                  AT %I101.0 : BOOL;
        //Output
        OvMotorTurnTableCW                  AT %Q100.0 : BOOL;
        OvMotorTurnTableCCW                 AT %Q100.1 : BOOL;
        OvMotorConvBeltForw                 AT %Q100.2 : BOOL;
        OvMotorSaw                          AT %Q100.3 : BOOL;
        OvMotorOvenFeederRetract            AT %Q100.4 : BOOL;
        OvMotorOvenFeederExtend             AT %Q100.5 : BOOL;
        OvMotorVacuumToOven                 AT %Q100.6 : BOOL;
        OvMotorVacuumToTurnTable            AT %Q100.7 : BOOL;
        OvLightOven                         AT %Q101.0 : BOOL;
        OvCompressor                        AT %Q101.1 : BOOL;
        OvValveVacuum                       AT %Q101.2 : BOOL;
        OvValveLowering                     AT %Q101.3 : BOOL;
        OvValveOpenDoor                     AT %Q101.4 : BOOL;
        OvValveFeeder                       AT %Q101.5 : BOOL;

        OvstatStep                          : INT;
        OvstatStartCounter                  : BOOL;
        OvstatCounterVal                    : INT;
    END_VAR

    VAR_GLOBAL//sorting
        //Input
        SlPulseCounter                      AT %I200.0 : BOOL;
        SlLightBarrierBeforeColourSensor    AT %I200.1 : BOOL;
        SlLightBarrierBehindColourSensor    AT %I200.2 : BOOL;
        SlLightBarrierWhite                 AT %I200.4 : BOOL;
        SlLightBarrierRed                   AT %I200.5 : BOOL;
        SlLightBarrierBlue                  AT %I200.6 : BOOL;
        SlSortLineColourSensor              AT %IW202  : INT;
        //Output
        SlMotorConveyorBelt                 AT %Q200.0 : BOOL;
        SlCompressor                        AT %Q200.1 : BOOL;
        SlValveEjectorWhite                 AT %Q200.3 : BOOL;
        SlValveEjectorRed                   AT %Q200.4 : BOOL;
        SlValveEjectorBlue                  AT %Q200.5 : BOOL;

        statStartCounter                    : BOOL;
        statTwoItemsOnBelt                  : BOOL;
        statColour                          : USInt;
        statStep                            : INT;
        statTimeCounter                     : INT;
        statMinColourVal                    : INT;
        statPosEdgeLightBarrierBeforeColourSensor : BOOL;
        statPosEdgeLightBarrierBehindColourSensor : BOOL;

        NotFirstCycle                       : BOOL;

        SlConfigData                        : SortLineConfig;
        SlPusher                            : ARRAY[1..3] OF Lib.cPusher;
        SlCompressorInst                    : Lib.cCompressor;
        SlCompressorIsRunning               : BOOL;
        SlCompressorError                   : BOOL;
    END_VAR         

    VAR_GLOBAL //Gripper
        //Input
        GrRefSwVertAxis                     AT %I300.0 : BOOL; 
        GrRefSwCantAxis                     AT %I300.1 : BOOL; 
        GrRefSwRotate                       AT %I300.2 : BOOL; 
        //Output                
        GrMotorVertAxisUp                   AT %Q300.0 : BOOL; 
        GrMotorVertAxisDown                 AT %Q300.1 : BOOL; 
        GrMotorCantAxisBackw                AT %Q300.2 : BOOL;
        GrMotorCantAxisForw                 AT %Q300.3 : BOOL; 
        GrMotorRotateCW                     AT %Q300.4 : BOOL; 
        GrMotorRotateCCW                    AT %Q300.5 : BOOL; 
        GrCompressor                        AT %Q300.6 : BOOL; 
        GrValveVacuum                       AT %Q300.7 : BOOL; 
        //Counter modules
        GrMotorVertAxisCountValue           AT %ID302 : DINT; 
        GrMotorVertAxisStatus               AT %ID314 : DINT;
        GrMotorVertAxisLoadValue            AT %QD302 : DINT; 
        GrMotorVertAxisControlValue         AT %QW311 : UINT; 
        GrMotorVertAxisControlByte          AT %QB310 : USINT; 
        //Counter module cantilever axis
        GrMotorCantAxisCountValue           AT %ID318 : DINT; 
        GrMotorCantAxisStatus               AT %ID330 : DINT; 
        GrMotorCantAxisLoadValue            AT %QD318 : DINT; 
        GrMotorCantAxisControlValue         AT %QW327 : UINT; 
        GrMotorCantAxisControlByte          AT %QB326 : USINT; 
        //counter module rotation axis
        GrMotorRotateAxisCountValue         AT %ID334 : DINT; 
        GrMotorRotateAxisStatus             AT %ID346 : DINT; 
        GrMotorRotateAxisLoadValue          AT %QD334 : DINT; 
        GrMotorRotateAxisControlValue       AT %QW343 : UINT; 
        GrMotorRotateAxisControlByte        AT %QB342 : USINT; 

        DriveCantileverRetVal               : INT;
        DriveCantileverExecute              : BOOL;
        DriveCantileverJog                  : BOOL;
        DriveCantileverCommand              : INT;
        DriveCantileverPosition             : DINT;
        DriveCantileverDirection            : INT;

        DriveCantileverBusy                 : BOOL;
        DriveCantileverDone                 : BOOL;
        DriveCantileverActiveCommand        : INT;

        DriveCantileverInternalState        : INT;
        DriveCantileverInternalCommand      : INT;
        DriveCantileverInternalDirection    : INT;
        DriveCantileverInternalPosition     : DINT;
        DriveCantileverInternalEdgeDetect   : BOOL;
        DriveCantileverInternalHomingState  : BOOL;

        //vars for function block drive vertical movement
        GrDriveVerticalRetVal               : INT;
        GrDriveVerticalExecute              : BOOL;
        GrDriveVerticalJog                  : BOOL;
        GrDriveVerticalCommand              : INT;
        GrDriveVerticalPosition             : DINT;
        GrDriveVerticalDirection            : INT;

        GrDriveVerticalBusy                 : BOOL;
        GrDriveVerticalDone                 : BOOL;
        GrDriveVerticalActiveCommand        : INT;

        GrDriveVerticalInternalState        : INT;
        GrDriveVerticalInternalCommand      : INT;
        GrDriveVerticalInternalDirection    : INT;
        GrDriveVerticalInternalPosition     : DINT;
        GrDriveVerticalInternalEdgeDetect   : BOOL;
        GrDriveVerticalInternalHomingState  : BOOL;

        //vars for function block drive rotation
        DriveRotationRetVal                 : INT;
        DriveRotationExecute                : BOOL;
        DriveRotationJog                    : BOOL;
        DriveRotationCommand                : INT;
        DriveRotationPosition               : DINT;
        DriveRotationDirection              : INT;

        DriveRotationBusy                   : BOOL;
        DriveRotationDone                   : BOOL;
        DriveRotationActiveCommand          : INT;

        DriveRotationInternalState          : INT;
        DriveRotationInternalCommand        : INT;
        DriveRotationInternalDirection      : INT;
        DriveRotationInternalPosition       : DINT;
        DriveRotationInternalEdgeDetect     : BOOL;
        DriveRotationInternalHomingState    : BOOL;
        
        //gripper positions
        GrPosSorting                        : Array[1..3] OF GripperPos;
        GrPosOven                           : GripperPos;
        GrPosWarehouse                      : GripperPos;
        GrPosPark                           : GripperPos;

        GrCycleStart                        : BOOL;
        GrCycleValue                        : INT;
        GrLoopCount                         : INT;
        GrstatStep                          : INT;
        GrstatState                         : INT;
        GrstatProcess                       : INT;
        GrstatDrivesHomed                   : BOOL;
        GrstatPosVertical                   : DINT;
        GrstatPosCantilever                 : DINT;
        GrstatPosRotation                   : DINT;
    END_VAR
END_CONFIGURATION

PROGRAM GeneralTasks
    VAR_EXTERNAL
        GlobalConfig                        : GlobalConfigDataType;

        SlConfigData                        : SortLineConfig;
        SlPusher                            : ARRAY[1..3] OF Lib.cPusher;
        SlCompressorInst                    : Lib.cCompressor;

        GrPosSorting                        : Array[1..3] OF GripperPos;
        GrPosOven                           : GripperPos;
        GrPosWarehouse                      : GripperPos;
        GrPosPark                           : GripperPos;

        WhPosBay1                           : WarehousePos;
        WhPosBayOffset                      : WarehousePos;
        WhPosPark                           : WarehousePos;
        WhPosTransfer                       : WarehousePos;
        WhPosDropOffset                     : WarehousePos;
    END_VAR
    VAR_TEMP
        loop : INT;
    END_VAR

    GlobalConfig.CallCycle_ms               := 10;
    GlobalConfig.PositionWindow             := DINT#3;
    //sortline configuration    
    SlConfigData.PusherExtendTime           := 100;
    SlConfigData.CompressorStartDelay       := 100;
    SlConfigData.CompressorTimeout          := 200;
    FOR loop := 1 TO 3 DO
        SlPusher[loop].mInit(ExtendTime_ms  := SlConfigData.PusherExtendTime, CycleTime_ms := GlobalConfig.CallCycle_ms);
    END_FOR;
    SlCompressorInst.mInit(SwitchOnTime_ms  := SlConfigData.CompressorStartDelay, CycleTime_ms := GlobalConfig.CallCycle_ms);

    //gripper configuration
    GrPosSorting[1].Vertical                := DINT#850;
    GrPosSorting[1].Cantilever              := DINT#358;
    GrPosSorting[1].Rotation                := DINT#-441;
    GrPosSorting[2].Vertical                := DINT#850;
    GrPosSorting[2].Cantilever              := DINT#395;
    GrPosSorting[2].Rotation                := DINT#-365;
    GrPosSorting[3].Vertical                := DINT#850;
    GrPosSorting[3].Cantilever              := DINT#546;
    GrPosSorting[3].Rotation                := DINT#-296;

    GrPosOven.Vertical                      := DINT#500;
    GrPosOven.Cantilever                    := DINT#860;
    GrPosOven.Rotation                      := DINT#-885;

    GrPosWarehouse.Vertical                 := DINT#250;
    GrPosWarehouse.Cantilever               := DINT#153;
    GrPosWarehouse.Rotation                 := DINT#-1337;

    GrPosPark.Vertical                      := DINT#0;
    GrPosPark.Cantilever                    := DINT#0;
    GrPosPark.Rotation                      := DINT#0;

    //Warehouse configuration           
    WhPosBay1.Horizontal                    := DINT#-1907;
    WhPosBay1.Vertical                      := DINT#30;
    WhPosBayOffset.Horizontal               := DINT#583;
    WhPosBayOffset.Vertical                 := DINT#368;
    WhPosPark.Horizontal                    := DINT#0;
    WhPosPark.Vertical                      := DINT#0;
    WhPosTransfer.Horizontal                := DINT#-10;
    WhPosTransfer.Vertical                  := DINT#650;
    WhPosDropOffset.Vertical                := DINT#30;
    WhPosDropOffset.Horizontal              := DINT#0;
END_PROGRAM