PROGRAM  Oven

    VAR_EXTERNAL
        //Input
        OvRefSwTurnTablePosVacuum   : Bool;
        OvRefSwTurnTablePosBelt     : Bool;
        OvLightBarrierEndOfConvBelt : Bool;
        OvRefSwTurnTablePosSaw      : Bool;
        OvRefSwVacuumPosTurnTable   : Bool;
        OvRefSwOvenFeederInside     : Bool;
        OvRefSwOvenFeederOutside    : Bool;
        OvRefSwVacuumPosOven        : Bool;
        OvLightBarrierOven          : Bool;
        //Output
        OvMotorTurnTableCW          : Bool;
        OvMotorTurnTableCCW         : Bool;
        OvMotorConvBeltForw         : Bool;
        OvMotorSaw                  : Bool;
        OvMotorOvenFeederRetract    : Bool;
        OvMotorOvenFeederExtend     : Bool;
        OvMotorVacuumToOven         : Bool;
        OvMotorVacuumToTurnTable    : Bool;
        OvLightOven                 : Bool;
        OvCompressor                : Bool;
        OvValveVacuum               : Bool;
        OvValveLowering             : Bool;
        OvValveOpenDoor             : Bool;
        OvValveFeeder               : Bool;
    END_VAR

    //local vars --> external due to error in compiler
    VAR_EXTERNAL
        OvstatStep                  : Int;
        OvstatStartCounter          : Bool;
        OvstatCounterVal            : Int;
    END_VAR    

    VAR_TEMP
        myBool : Bool;
    END_VAR 

    IF OvstatStep = 0 THEN
        //homing after start
        IF NOT OvRefSwOvenFeederOutside THEN
            OvstatStep := 6;
        ELSIF NOT OvRefSwVacuumPosTurnTable THEN
            OvstatStep := 14;
            OvMotorVacuumToTurnTable := TRUE;
            OvMotorTurnTableCCW := TRUE;
        //item on oven entry starts the process
        ELSIF NOT OvLightBarrierOven THEN
            OvstatStep := 1;
        END_IF;

    ELSIF OvstatStep = 1 THEN
        //open door
        OvCompressor := TRUE;
        OvValveOpenDoor := TRUE;
        OvstatStartCounter := TRUE;
        OvstatStep := 2;
            
    ELSIF OvstatStep = 2 THEN
        //wait for door opening
        IF OvstatCounterVal = 50 THEN
            OvstatStartCounter := FALSE;
            OvMotorOvenFeederRetract := TRUE;
            OvstatStep := 3;
        END_IF;
    
    ELSIF OvstatStep = 3 THEN
        //wait for end switch inside
        IF OvRefSwOvenFeederInside THEN
            OvMotorOvenFeederRetract := FALSE;
            OvCompressor := FALSE;
            OvValveOpenDoor := FALSE;
            OvstatStep := 4;
        END_IF;
    
    ELSIF OvstatStep = 4 THEN
        //prepare bake
        OvstatStartCounter := TRUE;
        OvLightOven := TRUE;
        OvstatStep := 5;
        IF NOT OvRefSwVacuumPosOven THEN
            OvMotorVacuumToOven := TRUE;
        END_IF;
    
    ELSIF OvstatStep = 5 THEN
        //baking finished
        IF OvstatCounterVal = 500 THEN
            OvLightOven := FALSE;
            OvstatStartCounter := FALSE;
            OvstatStep := 6;
        END_IF;
        IF OvRefSwVacuumPosOven THEN
            OvMotorVacuumToOven := FALSE;
        END_IF;
    
    ELSIF OvstatStep = 6 THEN
        //open door
        OvCompressor := TRUE;
        OvValveOpenDoor := TRUE;
        OvstatStartCounter := TRUE;
        OvstatStep := 7;

    ELSIF OvstatStep = 7 THEN
        //wait for door open, then move item outside
        IF OvstatCounterVal = 50 THEN
            OvstatStartCounter := FALSE;
            OvMotorOvenFeederExtend := TRUE;
            OvstatStep := 8;
        END_IF;
        IF OvRefSwVacuumPosOven THEN
            OvMotorVacuumToOven := FALSE;
        END_IF;

    ELSIF OvstatStep = 8 THEN
        //wait for end switch outside
        IF OvRefSwOvenFeederOutside THEN
            //stop motor
            OvMotorOvenFeederExtend := FALSE;
            //close door
            OvCompressor := FALSE;
            OvValveOpenDoor := FALSE;
            //if vacuum gripper is not in position, move it to pickup position
            IF NOT OvRefSwVacuumPosOven THEN
                OvMotorVacuumToOven := TRUE;
                OvstatStep := 9;
            ELSE
                OvstatStep := 10;
            END_IF;
        END_IF;
    
    ELSIF OvstatStep = 9 THEN
        //wait for vacuum gripper in oven pickup position
        IF OvRefSwVacuumPosOven THEN
            OvMotorVacuumToOven := FALSE;
            OvstatStep := 10;
        END_IF;

    ELSIF OvstatStep = 10 THEN
        //lower vacuum gripper
        OvCompressor := TRUE;
        OvValveLowering := TRUE;
        //start timer for moving process
        OvstatStartCounter := TRUE;
        OvstatStep := 11;
    
    ELSIF OvstatStep = 11 THEN
        //wait for lowering
        IF OvstatCounterVal = 80 THEN
            OvstatStartCounter := FALSE;
            OvValveVacuum := TRUE;
            OvValveLowering := FALSE;
            OvstatStep := 12;
        END_IF;
    
    ELSIF OvstatStep = 12 THEN
        //start Counter again for lifting
        OvstatStartCounter := TRUE;
        OvstatStep := 13;

    ELSIF OvstatStep = 13 THEN
        //check if item has been lifted, else try again
        IF OvstatCounterVal = 100 THEN
            OvstatStartCounter := FALSE;
            IF OvLightBarrierOven THEN
                //move vacuum gripper to drop position
                OvMotorVacuumToTurnTable := TRUE;
                //check for position of turntable
                IF NOT OvRefSwTurnTablePosVacuum THEN
                    OvMotorTurnTableCCW := TRUE;
                END_IF;
                OvstatStep := 14;
            ELSE
                OvValveVacuum := FALSE;
                OvstatStep := 10;
            END_IF;
        END_IF;

    ELSIF OvstatStep = 14 THEN
        //stop movement of turntable
        IF OvRefSwTurnTablePosVacuum THEN
            OvMotorTurnTableCCW := FALSE;
        END_IF;
        //wait for vacuum gripper in endpos
        IF OvRefSwVacuumPosTurnTable THEN
            //stop Motor
            OvMotorVacuumToTurnTable := FALSE;
            IF OvRefSwTurnTablePosVacuum THEN
                //lower vacuum gripper, start timer
                OvValveLowering := TRUE;
                OvstatStartCounter := TRUE;
                OvstatStep := 15;
            END_IF;
        END_IF;

    ELSIF OvstatStep = 15 THEN
        IF OvstatCounterVal = 80 THEN
            OvstatStartCounter := FALSE;
            //disable vacuum, move vacuum gripper up
            OvCompressor := FALSE;
            OvValveVacuum := FALSE;
            OvValveLowering := FALSE;
            OvstatStep := 16;
        END_IF;
    
    ELSIF OvstatStep = 16 THEN
        //start timer for lifting vacuum arm
        OvstatStartCounter := TRUE;
        OvstatStep := 17;

    ELSIF OvstatStep = 17 THEN
        IF OvstatCounterVal = 80 THEN
            OvstatStartCounter := FALSE;
            //start turntable
            OvMotorTurnTableCW := TRUE;
            OvstatStep := 18;
        END_IF;

    ELSIF OvstatStep = 18  THEN
        IF OvRefSwTurnTablePosSaw THEN
            //stop turntable
            OvMotorTurnTableCW := FALSE;
            //start saw
            OvMotorSaw := TRUE;
            //start counter for saw
            OvstatStartCounter := TRUE;
            OvstatStep := 19;
        END_IF;

    ELSIF OvstatStep = 19 THEN
        //wait for saw 
        IF OvstatCounterVal = 300 THEN
            OvstatStartCounter := FALSE;
            OvMotorSaw := FALSE;
            //move turntable to next position
            OvMotorTurnTableCW := TRUE;
            OvstatStep := 20;
        END_IF;

    ELSIF OvstatStep = 20 THEN
        IF OvRefSwTurnTablePosBelt THEN
            //stop turntable
            OvMotorTurnTableCW := FALSE;
            //start pusher onto conveyor
            OvCompressor := TRUE;
            OvValveFeeder := TRUE;
            //start counter for pushing time
            OvstatStartCounter := TRUE;
            OvstatStep := 21;
        END_IF;

    ELSIF OvstatStep = 21 THEN
        //wait for pushing done
        IF OvstatCounterVal = 80 THEN
            //disable pusher
            OvstatStartCounter := FALSE;
            OvCompressor := FALSE;
            OvValveFeeder := FALSE;
            //start conveyor for moving item to sorting machine
            OvMotorConvBeltForw := TRUE;
            OvstatStep := 22;
        END_IF;

    ELSIF OvstatStep = 22 THEN
        //restart timer
        OvstatStartCounter := TRUE;
        OvstatStep := 23;

    ELSIF OvstatStep = 23 THEN
        //light barrier at the end triggered, stay coasting for some time for secure handover
        IF OvLightBarrierEndOfConvBelt THEN
            OvstatCounterVal := 0;
            OvstatStep := 24;
        ELSIF OvstatCounterVal = 1200 THEN
            //timeout, probably item has been removed from conveyor, start over
            OvstatStep := 0;
        END_IF;

    ELSIF OvstatStep = 24 THEN
        IF OvstatCounterVal = 500 THEN
            OvstatStartCounter := FALSE;
            OvMotorConvBeltForw := FALSE;
            OvstatStep := 0;
        END_IF;
    END_IF;

        
IF OvstatStartCounter THEN
    OvstatCounterVal := OvstatCounterVal + 1;
ELSE
    OvstatCounterVal := 0;
END_IF;


END_PROGRAM