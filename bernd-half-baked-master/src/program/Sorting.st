PROGRAM Sorting
    //import hardware io
    VAR_EXTERNAL
        //Input
        SlPulseCounter                      : BOOL;
        SlLightBarrierBeforeColourSensor    : BOOL;
        SlLightBarrierBehindColourSensor    : BOOL;
        SlLightBarrierWhite                 : BOOL;
        SlLightBarrierRed                   : BOOL;
        SlLightBarrierBlue                  : BOOL;
        SlSortLineColourSensor              : INT;
        //Output
        SlMotorConveyorBelt                 : BOOL;
        SlCompressor                        : BOOL;
        SlValveEjectorWhite                 : BOOL;
        SlValveEjectorRed                   : BOOL;
        SlValveEjectorBlue                  : BOOL;
    END_VAR
    //import global data
    VAR_EXTERNAL
        GlobalConfig                        : GlobalConfigDataType;
        SlConfigData                        : SortLineConfig;
    END_VAR
    //global constants
    VAR_EXTERNAL
        ColourInvalid                       : USINT;
        ColourUndefined                     : USINT;
        ColourWhite                         : USINT;
        ColourRed                           : USINT;
        ColourBlue                          : USINT;
    END_VAR
    //local vars --> external due to error in compiler
    VAR_EXTERNAL
        statStartCounter                    : BOOL;
        statTwoItemsOnBelt                  : BOOL;
        statColour                          : USINT;
        statStep                            : INT;
        statTimeCounter                     : INT;
        statMinColourVal                    : INT;
        statPosEdgeLightBarrierBeforeColourSensor : BOOL;
        statPosEdgeLightBarrierBehindColourSensor : BOOL;

        SlCompressorIsRunning               : BOOL;
        SlCompressorError                   : BOOL;

        NotFirstCycle                       : BOOL;
        
        SlPusher                            : ARRAY[1..3] OF Lib.cPusher;

        SlCompressorInst                    : Lib.cCompressor;
    END_VAR

    VAR_TEMP
        tmpRequestCompressor : BOOL;
        tmpSuccess : BOOL;
        tmpLoop : INT;
    END_VAR
    //init all objects
    IF NOT NotFirstCycle THEN
        SlCompressorInst.mInit(
            SwitchOnTime_ms := SlConfigData.CompressorStartDelay
            ,CycleTime_ms := GlobalConfig.CallCycle_ms);
        FOR tmpLoop := 1 TO 3 DO
            SlPusher[tmpLoop].mInit(
                ExtendTime_ms := SlConfigData.PusherExtendTime
                ,CycleTime_ms := GlobalConfig.CallCycle_ms);
        END_FOR;
        NotFirstCycle := TRUE;
    END_IF;

    IF statStep = 0 THEN
        statStartCounter := FALSE;
        //init step
        IF NOT SlLightBarrierBeforeColourSensor THEN
            statStartCounter := TRUE;
            SlMotorConveyorBelt := TRUE;
            statMinColourVal := 25000;
            statStep := 1;
        END_IF;

    ELSIF statStep = 1 THEN
        //read minimum colour value while carrying item to second light barrier
        IF SlSortLineColourSensor < statMinColourVal THEN
            statMinColourVal := SlSortLineColourSensor;
        END_IF;
        //second light barrier reached, move item to storage bay
        IF  NOT SlLightBarrierBehindColourSensor THEN
            IF statMinColourVal < 10000 THEN //White
                statColour := ColourWhite;
            ELSIF statMinColourVal < 15000 THEN //red
                statColour := ColourRed;
            ELSIF statMinColourVal < 20000 THEN //blue
                statColour := ColourBlue;
            ELSE //only black recognized
                statColour := ColourUndefined;
            END_IF;
            statStep := 2;
            statTimeCounter := 0;
        END_IF;

        IF statTimeCounter = 1100 THEN
            //Timeout, probably item has been removed from conveyor
            statStartCounter := FALSE;
            //start over
            statStep := 0;
        END_IF;

        IF SlLightBarrierBeforeColourSensor AND NOT statPosEdgeLightBarrierBehindColourSensor THEN
            //a second item has been added while processing the first one. Has to be removed after sorting the first one
            statTwoItemsOnBelt := TRUE;
        END_IF;

    ELSIF statStep = 2 THEN
        //wait for the correct position of item, depending on waiting time from light barrier after colour sensor
        IF  statColour = ColourWhite      AND statTimeCounter = 50 OR
            statColour = ColourRed        AND statTimeCounter = 150 OR
            statColour = ColourBlue       AND statTimeCounter = 250 OR
            statColour = ColourUndefined  AND statTimeCounter = 320
        THEN
            statStartCounter := FALSE;
            SlMotorConveyorBelt := FALSE;
            IF statColour > ColourUndefined THEN //valid colour
                statStep := 3;
            ELSE
                statStep := 0; //undefined or invalid colour, item is moved to end of conveyor, then start over for next item
            END_IF;
        END_IF;

    ELSIF statStep = 3 THEN
        IF SlCompressorInst.mSwitchOn() THEN
            //switching on will be executed, now start the pusher
            statStep := 4;
        ELSE
            //switching on the compressor did not work --> start over
            statStep := 0;
        END_IF;
        

    ELSIF statStep = 4 THEN
        IF SlPusher[statColour - USINT#1].mPush(SlCompressorInst) THEN
            statStep := 5;
        END_IF;
    ELSIF statStep = 5 THEN
        IF NOT SlPusher[statColour - USINT#1].mIsPushActive() THEN
            SlCompressorInst.mSwitchOff();
            IF statTwoItemsOnBelt THEN
                statStep := 6;
            ELSE
                statStep := 0;
            END_IF;
        END_IF;

    ELSIF statStep = 6 THEN
        //two items are on conveyor, remove the second one
        statStartCounter := TRUE;
        SlMotorConveyorBelt := TRUE;
        statStep := 7;

    ELSIF statStep = 7 THEN
        IF statTimeCounter = 800 THEN
            SlMotorConveyorBelt := FALSE;
            statStartCounter := FALSE;
            statTwoItemsOnBelt := FALSE;
            statStep := 0;
        ELSIF SlLightBarrierBeforeColourSensor AND NOT statPosEdgeLightBarrierBeforeColourSensor THEN
            //another item has been dropped before conveyor was ready, remove this item as well
            //reset the time counter to start over
            statStartCounter := FALSE;
            //jump back to wait
            statStep := 6;
        END_IF;
    END_IF;

//time counter
IF statStartCounter THEN
    statTimeCounter := statTimeCounter + 1;
ELSE
    statTimeCounter := 0;
END_IF;

//cyclic compressor call
SlCompressorInst.mMainCyclic(HwOutCompressor => SlCompressor);
//cyclic pusher instance calls
SlPusher[1].mMainCyclic(HwOutPusher => SlValveEjectorWhite);
SlPusher[2].mMainCyclic(HwOutPusher => SlValveEjectorRed);
SlPusher[3].mMainCyclic(HwOutPusher => SlValveEjectorBlue);

//positive edge detection
statPosEdgeLightBarrierBeforeColourSensor := SlLightBarrierBeforeColourSensor;
statPosEdgeLightBarrierBehindColourSensor := SlLightBarrierBehindColourSensor;
     
END_PROGRAM