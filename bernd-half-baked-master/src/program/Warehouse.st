Program Warehouse
    VAR_EXTERNAL
        //Input
        WhRefSwitchHorAxis                  : BOOL;
        WhLightBarrierInside                : BOOL;
        WhLightBarrierOutside               : BOOL;
        WhRefSwitchVertAxis                 : BOOL;
        WhTrailSensor1Lower                 : BOOL;
        WhTrailSensor2Upper                 : BOOL;
        WhRefSwitchCantileverFront          : BOOL;
        WhRefSwitchCantileverBack           : BOOL;
        //Output
        WhMotorConvBeltForw                 : BOOL;
        WhMotorConvBeltBackw                : BOOL;
        WhMotorHorToRack                    : BOOL;
        WhMotorHorToConvBelt                : BOOL;
        WhMotorVertDown                     : BOOL;
        WhMotorVertUp                       : BOOL;
        WhMotorCantileverForw               : BOOL;
        WhMotorCantileverBackw              : BOOL;

        //Counter module horizontal axis
        WhMotorHorAxisCountValue            : DINT;  
        WhMotorHorAxisStatus                : DINT;  
        WhMotorHorAxisLoadValue             : DINT;  
        WhMotorHorAxisControlValue          : UInt;  
        WhMotorHorAxisControlByte           : USINT;
        //counter module vertical axis
        WhMotorVertAxisCountValue           : DINT;
        WhMotorVertAxisStatus               : DINT;
        WhMotorVertAxisLoadValue            : DINT;
        WhMotorVertAxisControlValue         : UInt;
        WhMotorVertAxisControlByte          : USINT;
    END_VAR
    
    //local vars --> external due to error in compiler
    VAR_EXTERNAL //drive controls
        //vars for function block drive cantilever
        DriveHorizontalRetVal               : INT;
        DriveHorizontalExecute              : BOOL;
        DriveHorizontalJog                  : BOOL;
        DriveHorizontalCommand              : INT;
        DriveHorizontalPosition             : DINT;
        DriveHorizontalDirection            : INT;
        DriveHorizontalBusy                 : BOOL;
        DriveHorizontalDone                 : BOOL;
        DriveHorizontalActiveCommand        : INT;
        DriveHorizontalInternalState        : INT;
        DriveHorizontalInternalCommand      : INT;
        DriveHorizontalInternalDirection    : INT;
        DriveHorizontalInternalPosition     : DINT;
        DriveHorizontalInternalEdgeDetect   : BOOL;
        DriveHorizontalInternalHomingState  : BOOL;

        //vars for function block drive vertical movement
        DriveVerticalRetVal                 : INT;
        DriveVerticalExecute                : BOOL;
        DriveVerticalJog                    : BOOL;
        DriveVerticalCommand                : INT;
        DriveVerticalPosition               : DINT;
        DriveVerticalDirection              : INT;
        DriveVerticalBusy                   : BOOL;
        DriveVerticalDone                   : BOOL;
        DriveVerticalActiveCommand          : INT;
        DriveVerticalInternalState          : INT;
        DriveVerticalInternalCommand        : INT;
        DriveVerticalInternalDirection      : INT;
        DriveVerticalInternalPosition       : DINT;
        DriveVerticalInternalEdgeDetect     : BOOL;
        DriveVerticalInternalHomingState    : BOOL;
    END_VAR

    VAR_EXTERNAL
        //global vars for communication between programs
        gboWareHouseItemReadyForPickup      : BOOL;
        gboWareHouseItemReadyForDrop        : BOOL;
        //global configuration data
        GlobalConfig                        : GlobalConfigDataType;
        //Warehouse configuration
        WhPosBay1                           : WarehousePos;
        WhPosBayOffset                      : WarehousePos;
        WhPosPark                           : WarehousePos;
        WhPosTransfer                       : WarehousePos;
        WhPosDropOffset                     : WarehousePos;
    END_VAR

    VAR_EXTERNAL //local vars
        WhStorage                           : Lib.WH.cWarehouseShelf;
        WhstatBayConfSet                    : BOOL;
        WhstatLoopNo                        : INT;
        WhstatStep                          : INT;
        WhstatState                         : INT;
        WhstatDrivesHomed                   : BOOL;
        WhstatPosVertical                   : DINT;
        WhstatPosHorizontal                 : DINT;
        WhstatSequencePosVertical           : DINT;
        WhstatSequencePosHorizontal         : DINT;
        WhstatCounterEnable                 : BOOL;
        WhstatCounterValue                  : INT;
    END_VAR

    VAR //simulate local constants
        //bay status: 0: invalid 1: empty 2: empty tray 3: filled
        BayFull                             : USINT := USINT#3;
        BayTray                             : USINT := USINT#2;
        BayEmpty                            : USINT := USINT#1;
        BayInvalid                          : USINT := USINT#0;
    END_VAR

    VAR_TEMP //local temps
        Error : BOOL;
    END_VAR

    //set warehouse properties once at restart
    IF NOT WhstatBayConfSet THEN
        WhstatLoopNo := 1;
        WhstatBayConfSet := TRUE;
    END_IF;

    //state machine for commands
    IF WhstatStep = 0 THEN // initial/idle step
        IF NOT DriveVerticalInternalHomingState OR
           NOT DriveHorizontalInternalHomingState
        THEN
            WhstatStep := 10;
        ELSE    
            IF WhstatState = 0 THEN
                IF WhstatLoopNo = 1 THEN
                    //clean item if inside hand-over station
                    IF NOT WhLightBarrierInside OR NOT WhLightBarrierOutside THEN
                        WhstatSequencePosHorizontal := WhPosBay1.Horizontal + WhPosBayOffset.Horizontal;
                        WhstatSequencePosVertical := WhPosBay1.Vertical;
                        WhstatState := 400;
                    ELSE
                        // 1: Full --> Empty
                        // 6: Tray --> Tray
                        WhstatSequencePosHorizontal := WhPosBay1.Horizontal;
                        WhstatSequencePosVertical := WhPosBay1.Vertical;
                        WhstatState := 100;
                        WhstatLoopNo := 2;
                    END_IF;
                ELSIF WhstatLoopNo = 2 THEN                        
                    // 1: Empty --> Tray
                    // 6: Tray --> Tray
                    WhstatSequencePosHorizontal := WhPosBay1.Horizontal;
                    WhstatSequencePosVertical := WhPosBay1.Vertical;
                    WhstatState := 300;
                    WhstatLoopNo := 3;
                ELSIF WhstatLoopNo = 3 THEN
                    // 1: Tray --> Tray
                    // 6: Tray --> Empty
                    WhstatSequencePosHorizontal := WhPosBay1.Horizontal + DINT#2 * WhPosBayOffset.Horizontal;
                    WhstatSequencePosVertical := WhPosBay1.Vertical + WhPosBayOffset.Vertical;
                    WhstatState := 200;
                    WhstatLoopNo := 4;                    
                ELSIF WhstatLoopNo = 4 THEN
                    // 1: Tray --> Tray
                    // 6: Empty --> Full
                    WhstatSequencePosHorizontal := WhPosBay1.Horizontal + DINT#2 * WhPosBayOffset.Horizontal;
                    WhstatSequencePosVertical := WhPosBay1.Vertical + WhPosBayOffset.Vertical;
                    WhstatState := 300;
                    WhstatLoopNo := 5;
                ELSIF WhstatLoopNo = 5 THEN
                    // 1: Tray --> Empty
                    // 6: Full --> Full
                    WhstatSequencePosHorizontal := WhPosBay1.Horizontal;
                    WhstatSequencePosVertical := WhPosBay1.Vertical;
                    WhstatState := 200;
                    WhstatLoopNo := 6;
                ELSIF WhstatLoopNo = 6 THEN
                    // 1: Empty --> Full
                    // 6: Full --> Full
                    WhstatSequencePosHorizontal := WhPosBay1.Horizontal;
                    WhstatSequencePosVertical := WhPosBay1.Vertical;
                    WhstatState := 300;
                    WhstatLoopNo := 7;
                ELSIF WhstatLoopNo = 7 THEN
                    // 1: Full --> Full
                    // 6: Full --> Empty
                    WhstatSequencePosHorizontal := WhPosBay1.Horizontal + DINT#2 * WhPosBayOffset.Horizontal;
                    WhstatSequencePosVertical := WhPosBay1.Vertical + WhPosBayOffset.Vertical;
                    WhstatState := 100;
                    WhstatLoopNo := 8;
                ELSIF WhstatLoopNo = 8 THEN
                    // 1: Full --> Full
                    // 6: Empty --> Tray
                    WhstatSequencePosHorizontal := WhPosBay1.Horizontal + DINT#2 * WhPosBayOffset.Horizontal;
                    WhstatSequencePosVertical := WhPosBay1.Vertical + WhPosBayOffset.Vertical;
                    WhstatState := 300;
                    WhstatLoopNo := 1;                
                END_IF;

            //steps:
            //20: Move arm
            //30: Drop item
            //40: Lift item
            //50: move outside
            //60: move inside
            //70: Exend cantilever
            //80: retract cantilever

            //deliver full
            ELSIF WhstatState = 100 THEN
                WhstatPosHorizontal := WhstatSequencePosHorizontal;
                WhstatPosVertical := WhstatSequencePosVertical + WhPosDropOffset.Vertical;
                WhstatStep := 20;
                WhstatState := 101;
            ELSIF WhstatState = 101 THEN
                WhstatStep := 70;
                WhstatState := 102;
            ELSIF WhstatState = 102 THEN
                WhstatStep := 40;
                WhstatState := 103;
            ELSIF WhstatState = 103 THEN
                WhstatPosHorizontal := WhPosTransfer.Horizontal;
                WhstatPosVertical := WhPosTransfer.Vertical;
                WhstatStep := 20;
                WhstatState := 104;
            ELSIF WhstatState = 104 THEN
                WhstatStep := 70;
                WhstatState := 105;
            ELSIF WhstatState = 105 THEN
                WhstatStep := 30;
                WhstatState := 106;
            ELSIF WhstatState= 106 THEN
                WhstatStep := 50;
                WhstatState := 107;
            ELSIF WhstatState = 107 THEN
                gboWareHouseItemReadyForPickup := TRUE;
                WhstatState := 108;
            ELSIF WhstatState = 108 THEN
                IF NOT gboWareHouseItemReadyForPickup THEN
                    WhstatState := 0;
                END_IF;

            //deliver empty
            ELSIF WhstatState = 200 THEN
                WhstatPosHorizontal := WhstatSequencePosHorizontal;
                WhstatPosVertical := WhstatSequencePosVertical + WhPosDropOffset.Vertical;
                WhstatStep := 20;
                WhstatState := 201;
            ELSIF WhstatState = 201 THEN
                WhstatStep := 70;
                WhstatState := 202;
            ELSIF WhstatState = 202 THEN
                WhstatStep := 40;
                WhstatState := 203;
            ELSIF WhstatState = 203 THEN
                WhstatPosHorizontal := WhPosTransfer.Horizontal;
                WhstatPosVertical := WhPosTransfer.Vertical;
                WhstatStep := 20;
                WhstatState := 204;
            ELSIF WhstatState = 204 THEN
                WhstatStep := 70;
                WhstatState := 205;
            ELSIF WhstatState = 205 THEN
                WhstatStep := 30;
                WhstatState := 206;
            ELSIF WhstatState = 206 THEN
                WhstatStep := 50;
                WhstatState := 207;
            ELSIF WhstatState = 207 THEN
                gboWareHouseItemReadyForDrop := TRUE;
                WhstatState := 208;
            ELSIF WhstatState = 208 THEN
                IF NOT gboWareHouseItemReadyForDrop THEN
                    WhstatState := 0;
                END_IF;

            //pickup
            ELSIF WhstatState = 300 THEN
                WhstatCounterEnable := TRUE;
                IF WhstatCounterValue >= 40 THEN
                    WhstatState := 301;
                END_IF;
            ELSIF WhstatState = 301 THEN
                WhstatStep := 60;
                WhstatState := 302;
            ELSIF WhstatState = 302 THEN
                WhstatStep := 40;
                WhstatState := 303;
            ELSIF WhstatState = 303 THEN
                WhstatPosHorizontal := WhstatSequencePosHorizontal;
                WhstatPosVertical := WhstatSequencePosVertical;
                WhstatStep := 20;
                WhstatState := 304;
            ELSIF WhstatState = 304 THEN
                WhstatStep := 70;
                WhstatState := 305;
            ELSIF WhstatState = 305 THEN
                WhstatStep := 30;
                WhstatState := 0;

        //clear hand-over station
            ELSIF WhstatState = 400 THEN
                WhstatPosHorizontal := WhPosTransfer.Horizontal;
                WhstatPosVertical := WhPosTransfer.Vertical + WhPosDropOffset.Vertical;
                WhstatStep := 20;
                WhstatState := 401;
            ELSIF WhstatState = 401 THEN
                WhstatStep := 70;
                WhstatState := 402;
            ELSIF WhstatState = 402 THEN
                WhstatStep := 60;
                WhstatState := 403;
            ELSIF WhstatState = 403 THEN
                WhstatStep := 40;
                WhstatState := 404;
            ELSIF WhstatState = 404 THEN
                WhstatPosHorizontal := WhstatSequencePosHorizontal;
                WhstatPosVertical := WhstatSequencePosVertical;
                WhstatStep := 20;
                WhstatState := 405;
            ELSIF WhstatState = 405 THEN
                WhstatStep := 70;
                WhstatState := 406;
            ELSIF WhstatState = 406 THEN
                WhstatStep := 30;
                WhstatState := 0;
            END_IF;
        END_IF;

    //-------------------- Homing --------------------
    ELSIF WhstatStep = 10 THEN
        IF WhRefSwitchCantileverBack THEN
            WhstatStep := 12;
        ELSE
            WhMotorCantileverBackw := TRUE;
            WhstatStep := 11;
        END_IF;
    ELSIF WhstatStep = 11 THEN
        IF WhRefSwitchCantileverBack THEN
            WhMotorCantileverBackw := FALSE;
            WhstatStep := 12;
        END_IF;
    ELSIF WhstatStep = 12 THEN
            DriveHorizontalExecute      := TRUE;
            DriveHorizontalCommand      := 1;
            DriveHorizontalDirection    := 1;
            DriveVerticalExecute        := TRUE;
            DriveVerticalCommand        := 1;
            DriveVerticalDirection      := 2;
            WhstatStep := 13;
    ELSIF WhstatStep = 13 THEN
        IF DriveHorizontalDone AND DriveVerticalDone THEN
            WhstatDrivesHomed         := TRUE;
            DriveHorizontalExecute  := FALSE;
            DriveVerticalExecute    := FALSE;
            WhstatStep                := 0;
        END_IF;
    //-------------------- move both axes to pos------------------
    ELSIF WhstatStep = 20 THEN
        //retract cantilever to prevent collisions
        IF WhRefSwitchCantileverBack THEN
            WhMotorCantileverBackw := FALSE;
            WhstatStep := 22;
        ELSE
            WhMotorCantileverBackw := TRUE;
            WhstatStep := 21;
        END_IF;

    ELSIF WhstatStep = 21 THEN
        IF WhRefSwitchCantileverBack THEN
            WhMotorCantileverBackw := FALSE;
            WhstatStep := 22;
        END_IF;

    ELSIF WhstatStep = 22 THEN
        //move both axes simoultaneously
        DriveHorizontalCommand := 3;
        DriveHorizontalPosition := WhstatPosHorizontal;
        DriveHorizontalExecute := TRUE;
        DriveVerticalCommand := 3;
        DriveVerticalPosition := WhstatPosVertical;
        DriveVerticalExecute := TRUE;
        WhstatStep := 23;
    ELSIF WhstatStep = 23 THEN
        //when both axes are done positioning, go back to start
        IF DriveHorizontalDone AND DriveVerticalDone THEN
           DriveHorizontalExecute := FALSE;
            DriveVerticalExecute := FALSE;
            WhstatStep := 0;
        END_IF;
    //-------------------- Drop Item -----------------------------
    ELSIF WhstatStep = 30 THEN
        DriveVerticalCommand := 2; //relative position
        DriveVerticalDirection := 1; //forward --> down
        DriveVerticalPosition := WhPosDropOffset.Vertical;
        DriveVerticalExecute := TRUE;
        WhstatStep := 31;
    ELSIF WhstatStep = 31 THEN
        IF DriveVerticalDone THEN
            DriveVerticalExecute := FALSE;
            WhstatStep := 0;
        END_IF;
    //-------------------- Lift Item -----------------------------
    ELSIF WhstatStep = 40 THEN
        DriveVerticalCommand := 2;
        DriveVerticalDirection := 2; //backward --> up
        DriveVerticalPosition := WhPosDropOffset.Vertical;
        DriveVerticalExecute := TRUE;
        WhstatStep := 41;
    ELSIF WhstatStep = 41 THEN
        IF DriveVerticalDone THEN
            DriveVerticalExecute := FALSE;
            WhstatStep := 0;
        END_IF;
    //-------------------- Move Item outside ---------------------
    ELSIF WhstatStep = 50 THEN
        WhMotorConvBeltForw := TRUE;
        IF NOT WhLightBarrierOutside THEN
            WhstatCounterEnable := TRUE;
            WhstatStep := 51;
        END_IF;
    ELSIF WhstatStep = 51 THEN
        IF WhstatCounterValue >= 20 THEN
            WhstatCounterEnable := FALSE;
            WhMotorConvBeltForw := FALSE;
            WhstatStep := 0;
        END_IF;
    //-------------------- Move Item inside ----------------------
    ELSIF WhstatStep = 60 THEN
        WhMotorConvBeltBackw := TRUE;
        IF NOT WhLightBarrierInside THEN
            WhMotorConvBeltBackw := FALSE;
            WhstatStep := 0;
        END_IF;
    //-------------------- Extend Cantilever ---------------------
    ELSIF WhstatStep = 70 THEN
        WhMotorCantileverForw := TRUE;
        IF WhRefSwitchCantileverFront THEN
            WhMotorCantileverForw := FALSE;
            WhstatStep := 0;
        END_IF;
    //-------------------- Retract Cantilever --------------------
    ELSIF WhstatStep = 80 THEN
        WhMotorCantileverBackw := TRUE;
        IF WhRefSwitchCantileverBack THEN
            WhMotorCantileverBackw := FALSE;
            WhstatStep := 0;
        END_IF;
    END_IF;

    IF WhstatCounterEnable THEN
        WhstatCounterValue := WhstatCounterValue + 1;
    ELSE
        WhstatCounterValue := 0;
    END_IF;

 DriveHorizontalRetVal := Lib.PosAxis(
    Execute                 := DriveHorizontalExecute,
    Jog                     := DriveHorizontalJog,
    Command                 := DriveHorizontalCommand,
    Position                := DriveHorizontalPosition,
    Direction               := DriveHorizontalDirection,
    PosWindow               := GlobalConfig.PositionWindow,
    //hardware inputs   
    HwInCounterVal          := WhMotorHorAxisCountValue,
    HwInCounterStatus       := WhMotorHorAxisStatus,
    HwInRefSw               := WhRefSwitchHorAxis,
    HwInEndSwFwd            := WhRefSwitchHorAxis,
    HwInEndSwBackw          := FALSE,

    //status outputs
    Busy                    => DriveHorizontalBusy,
    Error                   => Error,
    Done                    => DriveHorizontalDone,
    ActiveCommand           => DriveHorizontalActiveCommand,
    //hardware outputs
    HwOutCounterLoadVal     => WhMotorHorAxisLoadValue,
    HwOutCounterControlVal  => WhMotorHorAxisControlValue,
    HwOutCounterControlByte => WhMotorHorAxisControlByte,
    HwOutMotorFwd           => WhMotorHorToConvBelt,
    HwOutMotorBackw         => WhMotorHorToRack,
    //storage vars to know the current state
    InternalState           := DriveHorizontalInternalState,
    InternalCommand         := DriveHorizontalInternalCommand,
    InternalDirection       := DriveHorizontalInternalDirection,
    InternalPosition        := DriveHorizontalInternalPosition,
    InternalEdgeDetect      := DriveHorizontalInternalEdgeDetect,
    InternalHomingState     := DriveHorizontalInternalHomingState);

DriveVerticalRetVal := Lib.PosAxis(
    Execute                 := DriveVerticalExecute,
    Jog                     := DriveVerticalJog,
    Command                 := DriveVerticalCommand,
    Position                := DriveVerticalPosition,
    Direction               := DriveVerticalDirection,
    PosWindow               := GlobalConfig.PositionWindow,
    //hardware inputs   
    HwInCounterVal          := WhMotorVertAxisCountValue,
    HwInCounterStatus       :=WhMotorVertAxisStatus,
    HwInRefSw               := WhRefSwitchVertAxis, 
    HwInEndSwFwd            := FALSE,
    HwInEndSwBackw          := WhRefSwitchVertAxis,

    //status outputs
    Busy                    => DriveVerticalBusy,
    Error                   => Error,
    Done                    => DriveVerticalDone,
    ActiveCommand           => DriveVerticalActiveCommand,
    //hardware outputs
    HwOutCounterLoadVal     => WhMotorVertAxisLoadValue,
    HwOutCounterControlval  => WhMotorVertAxisControlValue,
    HwOutCounterControlByte => WhMotorVertAxisControlByte,
    HwOutMotorFwd           => WhMotorVertDown,
    HwOutMotorBackw         => WhMotorVertUp,

    //storage vars to know the current state
    InternalState           := DriveVerticalInternalState,
    InternalCommand         := DriveVerticalInternalCommand,
    InternalDirection       := DriveVerticalInternalDirection,
    InternalPosition        := DriveVerticalInternalPosition,
    InternalEdgeDetect      := DriveVerticalInternalEdgeDetect,
    InternalHomingState     := DriveVerticalInternalHomingState);
END_PROGRAM

