TYPE 
    GlobalConfigDataType : STRUCT
        CallCycle_ms : INT;
        PositionWindow : DINT;
    END_STRUCT;
        
END_TYPE

TYPE //sorting line
    PusherControlDataType : STRUCT
        ExecutePush : BOOL;
        PushTime_ms : INT;
    END_STRUCT;

    PusherStatusDataType : STRUCT  
        Busy : BOOL;
        Error : BOOL;
        Done : BOOL;
        ReqEnCompressor : BOOL;
    END_STRUCT;

    SortLineConfig : STRUCT
        PusherExtendTime        : INT;
        CompressorStartDelay    : INT;
        CompressorTimeout       : INT;
    END_STRUCT;
END_TYPE

TYPE //gripper
    GripperPos : STRUCT
        Cantilever : DINT;
        Vertical   : DINT;
        Rotation   : DINT;
    END_STRUCT;
END_TYPE

TYPE  //warehouse
     WarehousePos : STRUCT
        Horizontal : DINT;
        Vertical   : DINT;
    END_STRUCT;
END_TYPE