PROGRAM  Gripper
    VAR_EXTERNAL //own hardware interface
        //Input
        GrRefSwVertAxis             	    : BOOL; 
        GrRefSwCantAxis             	    : BOOL; 
        GrRefSwRotate               	    : BOOL; 
        //Output        	    
        GrMotorVertAxisUp           	    : BOOL; 
        GrMotorVertAxisDown         	    : BOOL; 
        GrMotorCantAxisBackw        	    : BOOL;
        GrMotorCantAxisForw         	    : BOOL; 
        GrMotorRotateCW             	    : BOOL;
        GrMotorRotateCCW            	    : BOOL; 
        GrCompressor                	    : BOOL; 
        GrValveVacuum               	    : BOOL; 

        //Counter modules
        GrMotorVertAxisCountValue           : DINT; 
        GrMotorVertAxisStatus               : DINT;
        GrMotorVertAxisLoadValue            : DINT; 
        GrMotorVertAxisControlValue         : UINT; 
        GrMotorVertAxisControlByte          : USINT; 

        GrMotorCantAxisCountValue           : DINT; 
        GrMotorCantAxisStatus               : DINT; 
        GrMotorCantAxisLoadValue            : DINT; 
        GrMotorCantAxisControlValue         : UINT; 
        GrMotorCantAxisControlByte          : USINT; 

        GrMotorRotateAxisCountValue         : DINT; 
        GrMotorRotateAxisStatus             : DINT; 
        GrMotorRotateAxisLoadValue          : DINT; 
        GrMotorRotateAxisControlValue       : UINT; 
        GrMotorRotateAxisControlByte        : USINT; 
    END_VAR

    VAR_EXTERNAL //Hardware inputs from other modules
        SlLightBarrierWhite                 : BOOL;
        SlLightBarrierRed                   : BOOL;
        SlLightBarrierBlue                  : BOOL;
        OvRefSwVacuumPosTurnTable           : BOOL;
        OvRefSwOvenFeederOutside            : BOOL;
    END_VAR

    VAR_EXTERNAL //global vars for communication between programs
        gboWareHouseItemReadyForPickup      : BOOL;
        gboWareHouseItemReadyForDrop        : BOOL;
    END_VAR

    VAR_EXTERNAL //configuration with constants
        //import global config values
        GlobalConfig                        : GlobalConfigDataType;
        //gripper positions 
        GrPosSorting                        : Array[1..3] OF GripperPos;
        GrPosOven                           : GripperPos;
        GrPosWarehouse                      : GripperPos;
        GrPosPark                           : GripperPos;        
    END_VAR

    //local vars --> external due to error in compiler
    VAR_EXTERNAL //drive controls
        //vars for function block drive cantilever
        DriveCantileverRetVal               : INT;
        DriveCantileverExecute              : BOOL;
        DriveCantileverJog                  : BOOL;
        DriveCantileverCommand              : INT;
        DriveCantileverPosition             : DINT;
        DriveCantileverDirection            : INT;

        DriveCantileverBusy                 : BOOL;
        DriveCantileverDone                 : BOOL;
        DriveCantileverActiveCommand        : INT;

        DriveCantileverInternalState        : INT;
        DriveCantileverInternalCommand      : INT;
        DriveCantileverInternalDirection    : INT;
        DriveCantileverInternalPosition     : DINT;
        DriveCantileverInternalEdgeDetect   : BOOL;
        DriveCantileverInternalHomingState  : BOOL;

        //vars for function block drive vertical movement
        GrDriveVerticalRetVal               : INT;
        GrDriveVerticalExecute              : BOOL;
        GrDriveVerticalJog                  : BOOL;
        GrDriveVerticalCommand              : INT;
        GrDriveVerticalPosition             : DINT;
        GrDriveVerticalDirection            : INT;

        GrDriveVerticalBusy                 : BOOL;
        GrDriveVerticalDone                 : BOOL;
        GrDriveVerticalActiveCommand        : INT;

        GrDriveVerticalInternalState        : INT;
        GrDriveVerticalInternalCommand      : INT;
        GrDriveVerticalInternalDirection    : INT;
        GrDriveVerticalInternalPosition     : DINT;
        GrDriveVerticalInternalEdgeDetect   : BOOL;
        GrDriveVerticalInternalHomingState  : BOOL;

        //vars for function block drive rotation
        DriveRotationRetVal                 : INT;
        DriveRotationExecute                : BOOL;
        DriveRotationJog                    : BOOL;
        DriveRotationCommand                : INT;
        DriveRotationPosition               : DINT;
        DriveRotationDirection              : INT;

        DriveRotationBusy                   : BOOL;
        DriveRotationDone                   : BOOL;
        DriveRotationActiveCommand          : INT;

        DriveRotationInternalState          : INT;
        DriveRotationInternalCommand        : INT;
        DriveRotationInternalDirection      : INT;
        DriveRotationInternalPosition       : DINT;
        DriveRotationInternalEdgeDetect     : BOOL;
        DriveRotationInternalHomingState    : BOOL;

     //local vars
		GrCycleStart                        : BOOL;
        GrCycleValue                        : INT;
        GrLoopCount                         : INT;
        GrstatStep                          : INT;
        GrstatState                         : INT;
        GrstatProcess                       : INT;
        GrstatDrivesHomed                   : BOOL;
        GrstatPosVertical                   : DINT;
        GrstatPosCantilever                 : DINT;
        GrstatPosRotation                   : DINT;
    END_VAR

    VAR_TEMP
        Error : BOOL;
        NoItem : BOOL;
    END_VAR

    GrLoopCount := GrLoopCount + 1;

	//state machine for commands
    IF GrstatStep = 0 THEN // initial/idle step
        IF NOT GrDriveVerticalInternalHomingState OR
           NOT DriveCantileverInternalHomingState OR 
           NOT DriveRotationInternalHomingState 
        THEN
            GrstatStep := 10;
        ELSE
            //positions and commands loop
            IF GrstatState = 0 THEN 
                IF gboWareHouseItemReadyForPickup THEN
                    //pickup item from warehosue
                    GrstatPosVertical := GrPosWarehouse.Vertical;
                    GrstatPosCantilever := GrPosWarehouse.Cantilever;
                    GrstatPosRotation := GrPosWarehouse.Rotation;
                    GrstatStep := 20;
                    GrstatState := 1;
                ELSIF gboWareHouseItemReadyForDrop THEN
                    IF NOT SlLightBarrierWhite THEN
                        GrstatPosVertical := GrPosSorting[1].Vertical;
                        GrstatPosCantilever := GrPosSorting[1].Cantilever;
                        GrstatPosRotation := GrPosSorting[1].Rotation;
                        GrstatState := 11;
                        GrstatStep := 20;
                    ELSIF NOT SlLightBarrierRed THEN
                        GrstatPosVertical := GrPosSorting[2].Vertical;
                        GrstatPosCantilever := GrPosSorting[2].Cantilever;
                        GrstatPosRotation := GrPosSorting[2].Rotation;
                        GrstatState := 11;
                        GrstatStep := 20;
                    ELSIF NOT SlLightBarrierBlue THEN
                        GrstatPosVertical := GrPosSorting[3].Vertical;
                        GrstatPosCantilever := GrPosSorting[3].Cantilever;
                        GrstatPosRotation := GrPosSorting[3].Rotation;
                        GrstatState := 11;
                        GrstatStep := 20;
                    ELSE
                        GrstatPosVertical := GrPosPark.Vertical;
                        GrstatPosCantilever := GrPosPark.Cantilever;
                        GrstatPosRotation := GrPosPark.Rotation;
                        GrstatStep := 20;
                        GrstatState := 20;
                    END_IF;
                ELSIF NOT SlLightBarrierWhite THEN
                    GrstatPosVertical := GrPosSorting[1].Vertical;
                    GrstatPosCantilever := GrPosSorting[1].Cantilever;
                    GrstatPosRotation := GrPosSorting[1].Rotation;
                    GrstatState := 1;
                    GrstatStep := 20;
                ELSIF NOT SlLightBarrierRed THEN
                    GrstatPosVertical := GrPosSorting[2].Vertical;
                    GrstatPosCantilever := GrPosSorting[2].Cantilever;
                    GrstatPosRotation := GrPosSorting[2].Rotation;
                    GrstatState := 1;
                    GrstatStep := 20;
                ELSIF NOT SlLightBarrierBlue THEN
                    GrstatPosVertical := GrPosSorting[3].Vertical;
                    GrstatPosCantilever := GrPosSorting[3].Cantilever;
                    GrstatPosRotation := GrPosSorting[3].Rotation;                    
                    GrstatState := 1;
                    GrstatStep := 20;
                ELSE
                    GrstatPosVertical := GrPosPark.Vertical;
                    GrstatPosCantilever := GrPosPark.Cantilever;
                    GrstatStep := 20;
                    GrstatState := 20;
                END_IF;

            ELSIF GrstatState = 1 OR GrstatState = 11 THEN
                GrstatStep := 50; //pull item
                IF GrstatState = 1 THEN
                    GrstatState := 2;                    
                    gboWareHouseItemReadyForPickup := FALSE;
                ELSIF GrstatState = 11 THEN
                    GrstatState := 12;
                END_IF;
            ELSIF GrstatState = 2 OR GrstatState = 12 THEN
                //move to wait position before oven
                IF GrstatState = 2 THEN
                    GrstatPosVertical := GrPosPark.Vertical;
                    GrstatPosCantilever := GrPosPark.Cantilever;
                    GrstatPosRotation := GrPosOven.Rotation;
                    GrstatStep := 20;
                    GrstatState := 3;
                //move to warehouse position for drop
                ELSIF GrstatState = 12 THEN
                    GrstatPosVertical := GrPosWarehouse.Vertical;
                    GrstatPosCantilever := GrPosWarehouse.Cantilever;
                    GrstatPosRotation := GrPosWarehouse.Rotation;
                    GrstatStep := 20;
                    GrstatState := 14;
                END_IF;
            ELSIF GrstatState = 3 THEN
                //if the drop position is clear, drop item
                IF OvRefSwVacuumPosTurnTable THEN
                    GrstatPosVertical := GrPosOven.Vertical;
                    GrstatPosCantilever := GrPosOven.Cantilever;
                    GrstatPosRotation := GrPosOven.Rotation;
                    GrstatStep := 20;
                    GrstatState := 4;
                END_IF;

            ELSIF GrstatState = 4 OR GrstatState = 14 THEN
                //dropped item into oven, start over
                IF GrstatState = 4 THEN
                    GrstatState := 0;
                //dropped item into warehouse, notify warehouse, then move
                ELSIF GrstatState = 14 THEN
                    gboWareHouseItemReadyForDrop := FALSE;
                    GrstatState := 15;
                END_IF;
                GrstatStep := 60;

            ELSIF GrstatState = 15 THEN                
                //after dropping item in warehouse, move away from warehouse
                GrstatPosVertical := GrPosPark.Vertical;
                GrstatPosCantilever := GrPosPark.Cantilever;
                GrstatStep := 20;
                GrstatState := 0;

            ELSIF GrstatState = 20 THEN
                IF NOT SlLightBarrierWhite OR
                   NOT SlLightBarrierRed OR
                   NOT SlLightBarrierBlue OR
                   gboWareHouseItemReadyForPickup
                THEN
                    GrstatState := 0;
                END_IF;
            END_IF;
        END_IF;
        
    //-------------------- Homing --------------------
    ELSIF GrstatStep = 10 THEN   
        GrDriveVerticalExecute        := TRUE;
        GrDriveVerticalCommand        := 1;
        GrDriveVerticalDirection      := 2;        
        GrstatStep                    := 11;
        
    ELSIF GrstatStep = 11 THEN
        IF GrDriveVerticalDone THEN
            GrDriveVerticalExecute    := FALSE;
            GrstatStep                := 12;
        END_IF;

    ELSIF GrstatStep = 12 THEN
        DriveCantileverExecute      := TRUE;
        DriveCantileverCommand      := 1;
        DriveCantileverDirection    := 2;
        GrstatStep                    := 13;

    ELSIF GrstatStep = 13 THEN
        IF DriveCantileverDone THEN
            DriveCantileverExecute  := FALSE;
            GrstatStep                := 14;
        END_IF;

    ELSIF GrstatStep = 14 THEN
        DriveRotationExecute        := TRUE;
        DriveRotationCommand        := 1;
        DriveRotationDirection      := 1;
        GrstatStep                    := 15;

    ELSIF GrstatStep = 15 THEN
        IF DriveRotationDone THEN
            GrstatDrivesHomed         := TRUE;
            DriveRotationExecute    := FALSE;
            GrstatStep                := 0;
            GrstatState               := 0;
        END_IF;

    //-------------------- Go to Position --------------------
    ELSIF GrstatStep = 20 THEN
        //move vertical and cantilever to 0 to prevent collisions
        GrDriveVerticalExecute := TRUE;
        GrDriveVerticalCommand := 3;//absolute pos
        GrDriveVerticalPosition := DINT#0;
        GrstatStep := 21;
    
    ELSIF GrstatStep = 21 THEN
        IF GrDriveVerticalDone THEN
            GrDriveVerticalExecute := FALSE;
            DriveCantileverExecute := TRUE;
            DriveCantileverCommand := 3; //absolute pos
            DriveCantileverPosition := DINT#0;
            GrstatStep := 22;
        END_IF;

    ELSIF GrstatStep = 22 THEN
        IF DriveCantileverDone THEN
            DriveCantileverExecute := FALSE;
            DriveRotationExecute := TRUE;
            DriveRotationCommand := 3;
            DriveRotationPosition := GrstatPosRotation;
            GrstatStep := 23;
        END_IF;

    ELSIF GrstatStep = 23 THEN
        IF DriveRotationDone THEN
            DriveRotationExecute := FALSE;
            DriveCantileverExecute := TRUE;
            DriveCantileverCommand := 3;
            DriveCantileverPosition := GrstatPosCantilever;
            GrstatStep := 24;
        END_IF;

    ELSIF GrstatStep = 24 THEN
        IF DriveCantileverDone THEN
            DriveCantileverExecute := FALSE;
            GrDriveVerticalExecute := TRUE;
            GrDriveVerticalCommand := 3;
            GrDriveVerticalPosition := GrstatPosVertical;
            GrstatStep := 25;
        END_IF;

    ELSIF GrstatStep = 25 THEN
        IF GrDriveVerticalDone THEN
            DriveCantileverExecute := FALSE;
            DriveRotationExecute := FALSE;
            GrDriveVerticalExecute := FALSE;
            GrstatStep := 0;
        END_IF;

    //-------------------- Pull Item --------------------
    ELSIF GrstatStep = 50 THEN
        GrCompressor := TRUE;
        GrValveVacuum := TRUE;
        GrstatStep := 0;

    //-------------------- Place Item --------------------
    ELSIF GrstatStep = 60 THEN
        GrCompressor := FALSE;
        GrValveVacuum := FALSE;
        GrstatStep := 0;
   END_IF;

    //function (block) call
    DriveCantileverRetVal := Lib.PosAxis(
        Execute                 := DriveCantileverExecute,
        Jog                     := DriveCantileverJog,
        Command                 := DriveCantileverCommand,
        Position                := DriveCantileverPosition,
        Direction               := DriveCantileverDirection,
        PosWindow               := GlobalConfig.PositionWindow,
        //hardware inputs   
        HwInCounterVal          := GrMotorCantAxisCountValue,
        HwInCounterStatus       := GrMotorCantAxisStatus,
        HwInRefSw               := GrRefSwCantAxis,
        HwInEndSwFwd            := FALSE,
        HwInEndSwBackw          := GrRefSwCantAxis,

        //status outputs
        Busy                    => DriveCantileverBusy,
        Error                   => Error,
        Done                    => DriveCantileverDone,
        ActiveCommand           => DriveCantileverActiveCommand,
        //hardware outputs
        HwOutCounterLoadVal     => GrMotorCantAxisLoadValue,
        HwOutCounterControlVal  => GrMotorCantAxisControlValue,
        HwOutCounterControlByte => GrMotorCantAxisControlByte,
        HwOutMotorFwd           => GrMotorCantAxisForw,
        HwOutMotorBackw         => GrMotorCantAxisBackw,

        //storage vars to know the current state
        InternalState           := DriveCantileverInternalState,
        InternalCommand         := DriveCantileverInternalCommand,
        InternalDirection       := DriveCantileverInternalDirection,
        InternalPosition        := DriveCantileverInternalPosition,
        InternalEdgeDetect      := DriveCantileverInternalEdgeDetect,
        InternalHomingState     := DriveCantileverInternalHomingState);

    GrDriveVerticalRetVal := Lib.PosAxis(
        Execute                 := GrDriveVerticalExecute,
        Jog                     := GrDriveVerticalJog,
        Command                 := GrDriveVerticalCommand,
        Position                := GrDriveVerticalPosition,
        Direction               := GrDriveVerticalDirection,
        PosWindow               := GlobalConfig.PositionWindow,
        //hardware inputs   
        HwInCounterVal          := GrMotorVertAxisCountValue,
        HwInCounterStatus       := GrMotorVertAxisStatus,
        HwInRefSw               := GrRefSwVertAxis, 
        HwInEndSwFwd            := FALSE,
        HwInEndSwBackw          := GrRefSwVertAxis,

        //status outputs
        Busy                    => GrDriveVerticalBusy,
        Error                   => Error,
        Done                    => GrDriveVerticalDone,
        ActiveCommand           => GrDriveVerticalActiveCommand,
        //hardware output   
        HwOutCounterLoadVal     => GrMotorVertAxisLoadValue,
        HwOutCounterControlval  => GrMotorVertAxisControlValue,
        HwOutCounterControlByte => GrMotorVertAxisControlByte,
        HwOutMotorFwd           => GrMotorVertAxisDown,
        HwOutMotorBackw         => GrMotorVertAxisUp,

        //storage vars to know the current state
        InternalState           := GrDriveVerticalInternalState,
        InternalCommand         := GrDriveVerticalInternalCommand,
        InternalDirection       := GrDriveVerticalInternalDirection,
        InternalPosition        := GrDriveVerticalInternalPosition,
        InternalEdgeDetect      := GrDriveVerticalInternalEdgeDetect,
        InternalHomingState     := GrDriveVerticalInternalHomingState);

    DriveRotationRetVal := Lib.PosAxis(
        Execute                 := DriveRotationExecute,
        Jog                     := DriveRotationJog,
        Command                 := DriveRotationCommand,
        Position                := DriveRotationPosition,
        Direction               := DriveRotationDirection,
        PosWindow               := GlobalConfig.PositionWindow,
        //hardware inputs   
        HwInCounterVal          := GrMotorRotateAxisCountValue,
        HwInCounterStatus       := GrMotorRotateAxisStatus,
        HwInRefSw               := GrRefSwRotate,
        HwInEndSwFwd            := GrRefSwRotate,
        HwInEndSwBackw          := FALSE,

        //status outputs
        Busy                    => DriveRotationBusy,
        Error                   => Error,
        Done                    => DriveRotationDone,
        ActiveCommand           => DriveRotationActiveCommand,
        //hardware outputs
        HwOutCounterLoadVal     => GrMotorRotateAxisLoadValue,
        HwOutCounterControlVal  => GrMotorRotateAxisControlValue,
        HwOutCounterControlByte => GrMotorRotateAxisControlByte,
        HwOutMotorFwd           => GrMotorRotateCW,
        HwOutMotorBackw         => GrMotorRotateCCW,

        //storage vars to know the current state
        InternalState           := DriveRotationInternalState,
        InternalCommand         := DriveRotationInternalCommand,
        InternalDirection       := DriveRotationInternalDirection,
        InternalPosition        := DriveRotationInternalPosition,
        InternalEdgeDetect      := DriveRotationInternalEdgeDetect,
        InternalHomingState     := DriveRotationInternalHomingState);

    IF GrCycleStart THEN
        GrCycleValue := GrCycleValue + 1;
    ELSE
        GrCycleValue := 0;
    END_IF;
END_PROGRAM