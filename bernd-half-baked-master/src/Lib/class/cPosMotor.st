NAMESPACE Lib
    CLASS cPosMotor
        VAR PRIVATE
            state       : USInt;
            PosTol      : USINT := USINT#3;
            HomingDir   : USINT;
            Homed       : Bool;
            PosDistance : Int;
            AbsPos      : Int;
        END_VAR

        //replacement for symbolic constants or ENUM until they are available
        VAR PRIVATE
            STATE_IDLE                  : USINT := USINT#0;
            STATE_HOMING                : USINT := USINT#10;
            STATE_POS_RELATIVE_START    : USINT := USINT#20;

            DIRECTION_UNDEFINED     : USINT := USINT#0;
            DIRECTION_FORWARD       : USINT := USINT#1;
            DIRECTION_BACKWARD      : USINT := USINT#2;
        END_VAR

        METHOD PUBLIC mInit : BOOL
            VAR_INPUT
                PosWindow : USINT := USINT#3;
                HomingDirection : USINT;
            END_VAR
            IF HomingDirection = DIRECTION_FORWARD OR HomingDirection = DIRECTION_BACKWARD THEN
                HomingDir := HomingDirection;
                PosTol := PosWindow;
                mInit := TRUE;
            ELSE
                mInit := FALSE;
            END_IF;
        END_METHOD

        METHOD PUBLIC mCfgValid : Bool
            IF HomingDir <> DIRECTION_UNDEFINED THEN
                mCfgValid := TRUE;
            ELSE   
                mCfgValid := FALSE;
            END_IF;
        END_METHOD

        METHOD PUBLIC mIsIdle : Bool
            mIsIdle := state = STATE_IDLE;
        END_METHOD

        METHOD PUBLIC mHome : BOOL
            VAR_OUTPUT
            END_VAR
            IF state = STATE_IDLE THEN
                IF THIS.mCfgValid() THEN
                    state := USINT#1;
                    mHome := TRUE;
                ELSE
                    mHome := FALSE;
                END_IF;
            ELSE
                mHome := FALSE;
            END_IF;        
        END_METHOD

        METHOD PUBLIC mPosRelative : Bool
            VAR_INPUT
                Distance : Int;
            END_VAR
            IF Distance <> 0 AND THIS.mCfgValid() AND state = STATE_IDLE THEN
                PosDistance := Distance;
                mPosRelative := TRUE;
            ELSE
                mPosRelative := FALSE;
            END_IF;
        END_METHOD

        METHOD mPosAbsolute : Bool
            VAR_INPUT
                Position : Int;
            END_VAR
            IF THIS.mCfgValid() AND Homed AND state = STATE_IDLE THEN
                AbsPos := Position;
                mPosAbsolute := TRUE;
            ELSE
                mPosAbsolute := FALSE;
            END_IF;
        END_METHOD

        METHOD PUBLIC mMainCyclic
            VAR_INPUT
                CounterRead : CounterModuleFeedbackInterface;
                RefSwitch           : Bool;
                EndSwitchForward    : Bool;
                EndSwitchBackward   : Bool;
            END_VAR
            VAR_OUTPUT
                CounterWrite : CounterModuleControlInterface;
                MotorForward    : Bool;
                MotorBackward   : Bool;
            END_VAR
            VAR_TEMP
                tmpWriteCounter : CounterModuleControlInterface;
                tmpMotorForward : Bool;
                tmpMotorBackward : Bool;
                tmpMovePossible : Bool;
                tmpAbsolutePos  : Int;
            END_VAR

            IF state = STATE_IDLE THEN
                ;//wait for commands from methods
            ELSIF state = STATE_HOMING THEN
                //ref while ref switch is not reached
                IF NOT RefSwitch THEN
                    tmpMovePossible := THIS.mControlDigitalMotor(HomingDir, EndSwitchForward, EndSwitchBackward, tmpMotorForward, tmpMotorBackward);
                    IF NOT tmpMovePossible THEN
                        state := STATE_IDLE;
                    END_IF;
                ELSE
                    state := STATE_IDLE;
                END_IF;
            ELSIF state = STATE_POS_RELATIVE_START THEN
                //calculate absolute Position
                tmpAbsolutePos := PosDistance - DINT_TO_INT(CounterRead.CounterValue);
                ;   
            ELSE
                ;
            END_IF;

            //write temp values to hardware output
            MotorForward := tmpMotorForward;
            MotorBackward := tmpMotorBackward;
            //basic settings for counter Counter module
            tmpWriteCounter.ControlBits.SwGate := TRUE; //always enable the sw-gate
            //write complete structure
            CounterWrite := tmpWriteCounter;
        END_METHOD

        METHOD PRIVATE mControlDigitalMotor : Bool
            VAR_INPUT
                Direction : USINT;
                EndSwitchForward : Bool;
                EndSwitchBackward : Bool;
            END_VAR
            VAR_OUTPUT
                MotorForward : Bool;
                MotorBackward : Bool;
            END_VAR
            VAR_TEMP
                tempForward : Bool;
                tempBackward : Bool;
            END_VAR
            tempForward := Direction = DIRECTION_FORWARD AND NOT EndSwitchForward;
            tempBackward := Direction = DIRECTION_BACKWARD AND NOT EndSwitchBackward;
            mControlDigitalMotor := tempForward OR tempBackward;
            MotorForward := tempForward;
            MotorBackward := tempBackward;
        END_METHOD
        
    END_CLASS

    //strucures for communication interface to counter module
    TYPE
        CounterModuleControlInterface : STRUCT
            LoadValue0  : DWORD;
            LoadValue1  : DWORD;
            ControlByte : BYTE;
            ControlBits : CounterModuleControlBits;    
        END_STRUCT;

        CounterModuleFeedbackInterface : STRUCT
            CounterValue    : DINT;
            CapturedValue   : DINT;
            MeasuredValue   : REAL;
            StatusBits      : CounterModuleFeedbackBits;
        END_STRUCT;

        CounterModuleControlBits : STRUCT
            SwGate      : BOOL;
            TmCtrlDQ0   : BOOL;
            TmCtrlDQ1   : BOOL;
            SetDQ0      : BOOL;
            SetDQ1      : BOOL;
            EnSyncUp    : BOOL;
            EnSyncDown  : BOOL;
            EnCapture   : BOOL;
            ResError    : BOOL;
            ResEvent    : BOOL;
            Reserved0   : BOOL;
            Reserved1   : BOOL;
            Reserved2   : BOOL;
            Reserved3   : BOOL;
            Reserved4   : BOOL;
            SetDir      : BOOL;
            Reserved5   : BOOL;
        END_STRUCT;

        CounterModuleFeedbackBits : STRUCT
            PowerError      : BOOL;
            EncError        : BOOL;
            IdError         : BOOL;
            Reserved1       : BOOL;
            Reserved2       : BOOL;
            Reserved3       : BOOL;
            Reserved4       : BOOL;
            Reserved5       : BOOL;
            CommActive      : BOOL;
            ResEventAck     : BOOL;
            IdStsSlot0      : BOOL;
            IdStsSlot1      : BOOL;
            StsReady        : BOOL;
            StsSwGate       : BOOL;
            Reserved7       : BOOL;
            Reserved8       : BOOL;
            StsDir          : BOOL;
            StsCount        : BOOL;
            StsGate         : BOOL;
            StsDQ0          : BOOL;
            StsDQ1          : BOOL;
            StsDI0          : BOOL;
            StsDI1          : BOOL;
            StsDI2          : BOOL;
            EventZero       : BOOL;
            EventUnderflo   : BOOL;
            EventOverflow   : BOOL;
            EventCompare0   : BOOL;
            EventCompare1   : BOOL;
            EventSync       : BOOL;
            EventCapture    : BOOL;
            StsMeasInterval : BOOL;
        END_STRUCT;
    END_TYPE
END_NAMESPACE