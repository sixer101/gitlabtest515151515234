NAMESPACE Lib
    CLASS cCompressor
        VAR PRIVATE //configuration data
            SwitchonTime    : Int;
            CycleTime       : Int;
        END_VAR

        VAR PRIVATE //internal data
            CounterEnable : Bool;
            CounterValue  : Int;
            State         : Int;
            CmdOn         : Bool;
            IsRunning     : Bool;
        END_VAR

        VAR PRIVATE //outputs
            oCompressor : Bool;
        END_VAR

        //initialise internal configuration values
        //negative return value indicates invalid config values
        METHOD PUBLIC mInit : Bool
            VAR_INPUT
                SwitchOnTime_ms : Int;
                CycleTime_ms    : Int;
            END_VAR
            IF SwitchOnTime_ms > 0 AND
                CycleTime_ms   > 0
            THEN
                SwitchonTime    := SwitchOnTime_ms;
                CycleTime       := CycleTime_ms;
                mInit           := TRUE;
            ELSE
                mInit := FALSE;
            END_IF;
        END_METHOD

        METHOD PRIVATE mCheckConfigValues : Bool
            mCheckConfigValues := SwitchOnTime > 0 AND CycleTime > 0;
        END_METHOD

        //command for switching on
        METHOD PUBLIC mSwitchOn : Bool
            IF NOT THIS.mCheckConfigValues() THEN
                mSwitchOn := FALSE;
            ELSE
                CmdOn     := TRUE;
                mSwitchOn := TRUE;
            END_IF;
        END_METHOD

        //command for switching off
        METHOD PUBLIC mSwitchOff
            CmdOn := FALSE;
        END_METHOD

        METHOD PUBLIC IsActive : Bool
            IsActive := IsRunning;
        END_METHOD

        METHOD PUBLIC mMainCyclic
            VAR_OUTPUT
                HwOutCompressor : Bool;
            END_VAR
            //logic for switch on delay
            IF State = 0 THEN
                IF CmdOn THEN
                    oCompressor := TRUE;
                    CounterEnable := TRUE;
                    State := 1;
                END_IF;
            ELSIF State = 1 THEN
                IF CmdOn THEN
                    IF CounterValue * CycleTime >= SwitchOnTime THEN
                        IsRunning := TRUE;
                        CounterEnable := FALSE;
                        State := 2;
                    END_IF;
                ELSE
                    CounterEnable := FALSE;
                    oCompressor := FALSE;
                    State := 0;
                END_IF;
            ELSIF State = 2 THEN
                IF NOT CmdOn THEN
                    oCompressor := FALSE;
                    IsRunning := FALSE;
                    State := 0;
                END_IF;
            END_IF;
            //count call cycles
            IF CounterEnable THEN
                CounterValue := CounterValue + 1;
            ELSE
                CounterValue := 0;
            END_IF;
            //write outputs
            HwOutCompressor := oCompressor;
        END_METHOD
    END_CLASS
END_NAMESPACE