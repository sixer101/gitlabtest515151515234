NAMESPACE Lib.WH
    CLASS cWarehouseShelf
        VAR PRIVATE
            HorNoArrayStart     : Int := 1;
            HorNoArrayEnd       : Int := 3;
            VertNoArrayStart    : Int := 1;
            VertNoArrayEnd      : Int := 3;
        END_VAR
        
        VAR PRIVATE //OVERRIDE
            Shelf : WarehouseStorageType;
        END_VAR

        METHOD PUBLIC mWriteItem : BOOL
            VAR_INPUT
                Row         : Int;
                Column      : Int;
                ItemType    : WarehouseElement;
            END_VAR
                IF THIS.mCheckBoundaries(Row, Column) AND (ItemType.State > USINT#0) AND (ItemType.State <= USINT#3) THEN
                    Shelf[Row,Column] := ItemType;
                    mWriteItem := TRUE;
                ELSE
                    mWriteItem := FALSE;
                END_IF;                
        END_METHOD

        METHOD PRIVATE mCheckBoundaries : BOOL
            VAR_INPUT
                Row     : INT;
                Column  : INT;
            END_VAR
                mCheckBoundaries := 
                    Row     >= HorNoArrayStart    AND 
                    Row     <= HorNoArrayEnd      AND 
                    Column  >= VertNoArrayStart   AND 
                    Column  <= VertNoArrayEnd;
            END_METHOD

        METHOD PUBLIC mSearchInsideShelf : WarehouseElementPos
            VAR_INPUT
                Element : WarehouseElement;
            END_VAR
            VAR_TEMP
                Row     : Int;
                Column  : Int;
                Hit     : Bool;
                Pos     : WarehouseElementPos;
            END_VAR

            FOR Column := VertNoArrayStart TO VertNoArrayEnd DO
                FOR Row := HorNoArrayStart TO HorNoArrayEnd DO
                    IF Shelf[Row, Column].State = Element.State AND Shelf[Row, Column].Colour = Element.Colour OR
                        Shelf[Row, Column].State = Element.State AND Element.State <> USINT#3
                    THEN
                        Pos.Row := Row;
                        Pos.Column := Column;
                        Hit := TRUE;
                        EXIT;
                    END_IF;
                END_FOR;
                IF Hit THEN EXIT; END_IF;
            END_FOR;
            mSearchInsideShelf := Pos;
        END_METHOD
    END_CLASS

    TYPE
    WarehouseStorageType : Array[1..3,1..3] of WarehouseElement;

    WarehouseElement : STRUCT
        State   : USINT;
        Colour  : USINT;
    END_STRUCT;

    WarehouseElementPos : STRUCT
        Row     : INT;
        Column  : INT;
    END_STRUCT;

END_TYPE

END_NAMESPACE

/*
//maybe possible in the future:

INTERFACE Comparer
    METHOD PUBLIC Compare : BOOL
        VAR_INPUT
            CompareValue : Int;
            ReferenceValue : Int;
        END_VAR
        ;
    END_METHOD
END_INTERFACE

CLASS GreaterThan IMPLEMENTS Comparer
    METHOD PUBLIC Compare : BOOL
        Compare := CompareValue > ReferenceValue;
    END_METHOD
END_CLASS

CLASS SmallerThan IMPLEMENTS Comparer
    METHOD PUBLIC Compare : BOOL
        Compare := CompareValue < ReferenceValue;
    END_METHOD
END_CLASS

CLASS Equal IMPLEMENTS Comparer
    METHOD PUBLIC Compare : BOOL
        Compare := CompareValue = ReferenceValue;
    END_METHOD
END_CLASS

METHOD PRIVATE mSearchInsideShelf : WarehouseElementPos
    VAR_INPUT
        Value1 : Int;
        Value2 : Int;
        Comparer1 : Comparer;
        Comparer2 : Comparer;
    END_VAR
    VAR_TEMP
        Row     : Int;
        Column  : Int;
    END_VAR
    FOR Column := VertNoArrayStart TO VertNoArrayEnd DO
        FOR Row := HorNoArrayStart TO HorNoArrayEnd DO
            IF Comparer1.Compare(CompareValue := Value1, ReferenceValue := WarehouseElementPos[row,column].State) AND 
               Comparer2.Compare(CompareValue := Value2, ReferenceValue := WarehouseElementPos[row, column].Colour) 
            THEN
                WarehouseElementPos.HorizontalNo := Row;
                WarehouseElementPos.VerticalNo := Column;
                EXIT;
            END_IF;
        END_FOR;
    END_FOR;    
END_METHOD

mSearchInsideShelf(full, white, equal, greaterthan)
*/