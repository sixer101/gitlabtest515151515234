NAMESPACE Lib
    CLASS cPusher
        VAR PRIVATE //config vars
            ExtendTime      : Int;
            CycleTime       : Int;
        END_VAR

        VAR PRIVATE //local vars
            PushJob         : Bool;
            CounterEnable   : Bool;
            CounterValue    : Int;
            State           : Int;
        END_VAR

        VAR PRIVATE //outputs
            oPusher : Bool;
        END_VAR

        //initialise internal configuration values
        //negative return value indicates invalid config values
        METHOD PUBLIC mInit : Bool
            VAR_INPUT
                ExtendTime_ms : Int;
                CycleTime_ms  : Int;
            END_VAR
            IF ExtendTime_ms > 0 AND  
                CycleTime_ms > 0
            THEN
                ExtendTime := ExtendTime_ms;
                CycleTime := CycleTime_ms;
                mInit := TRUE;
            ELSE
                mInit := FALSE;
            END_IF;
        END_METHOD

        METHOD PRIVATE mCheckConfigValues : BOOL
            mCheckConfigValues := ExtendTime > 0 AND CycleTime > 0;
        END_METHOD

        METHOD PUBLIC mPush : Bool
            VAR_IN_OUT
                CompressorInstance : cCompressor;
            END_VAR
            IF THIS.mCheckConfigValues() AND CompressorInstance.IsActive() THEN
                PushJob := TRUE;
                mPush := TRUE;
            ELSE
                mPush := FALSE;
            END_IF;
        END_METHOD

        //status information if pusher is busy
        METHOD PUBLIC mIsPushActive : Bool
            mIsPushActive := PushJob;
        END_METHOD

        METHOD PUBLIC mMainCyclic
            VAR_OUTPUT
                HwOutPusher : Bool;
            END_VAR

            IF State = 0 THEN
                IF PushJob THEN
                    oPusher := TRUE;
                    CounterEnable := TRUE;
                    State := 1;
                END_IF;
            ELSIF State = 1 THEN
                IF CounterValue * CycleTime >= ExtendTime THEN
                    CounterEnable := FALSE;
                    oPusher := FALSE;
                    PushJob := FALSE;
                    State := 0;
                END_IF;
            END_IF;
            //count call cycles
            IF CounterEnable THEN
                CounterValue := CounterValue + 1;
            ELSE
                CounterValue := 0;
            END_IF;
            //write outputs
            HwOutPusher := oPusher;
        END_METHOD
    END_CLASS
END_NAMESPACE