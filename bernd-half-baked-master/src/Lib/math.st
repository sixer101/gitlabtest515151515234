NAMESPACE Math
    FUNCTION Multiplication : Int
        VAR_INPUT
            mul1 : Int;
            mul2 : Int;
        END_VAR
        VAR_TEMP
        END_VAR

        Multiplication := mul1 * mul2;
    END_FUNCTION 

    FUNCTION Addition : Int
        VAR_INPUT
            summand1 : Int;
            summand2 : Int;
        END_VAR
        VAR_TEMP
        END_VAR

        Addition := summand1 + summand2;
    END_FUNCTION
END_NAMESPACE 