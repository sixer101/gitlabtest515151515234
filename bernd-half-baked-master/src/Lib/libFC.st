NAMESPACE Lib
    
FUNCTION PosAxis : Int
    
    VAR_INPUT
        //control inputs
        Execute             : Bool;
        Jog                 : Bool;
        Command             : Int;
        Position            : DInt;
        Direction           : Int;
        PosWindow           : DInt;
        //hardware inputs
        HwInCounterVal      : DInt;
        HwInCounterStatus   : DInt;
        HwInRefSw           : Bool;
        HwInEndSwFwd        : Bool;
        HwInEndSwBackw      : Bool;
    END_VAR

    VAR_OUTPUT
        //status outputs
        Busy                    : Bool;
        Error                   : Bool;
        Done                    : Bool;
        ActiveCommand           : Int;
        //hardware outputs
        HwOutCounterLoadVal     : DInt;
        HwOutCounterControlVal  : UInt;
        HwOutCounterControlByte : USInt;
        HwOutMotorFwd           : Bool;
        HwOutMotorBackw         : Bool;
    END_VAR

    VAR_IN_OUT
        //storage vars to know the current state
        InternalState       : Int;
        InternalCommand     : Int;
        InternalDirection   : Int;
        InternalPosition    : DInt;
        InternalEdgeDetect  : Bool;
        InternalHomingState : Bool;
    END_VAR
    
    VAR_TEMP
        tempMotorFwd : Bool;
        tempMotorBackw : Bool;
        tempCounterControlBits : UInt;
        tempCounterControlByte : USint;
        tempErrorID : Int;
    END_VAR

    //software gate is always open, it is bit 0, binary value --> 1
    tempCounterControlBits := tempCounterControlBits + UInt#256;
    //Home position is always 0 for now
    HwOutCounterLoadVal := DInt#0;
    
    //InternalState is separated:
    //Each command has 10 steps, Step 0 is initial Step, Step 9 is the done step.
    //stepcase is in between those values, offset value between commands is 10

    //idle --> no command is active, wait for new trigger
    IF InternalState = 0 THEN //idle
        //positive edge on execute starts functionality
        //command has to be exclusive, not jog and execute simulaneously
        IF Execute AND NOT InternalEdgeDetect AND NOT Jog THEN
            //check for empty command, this will give back an error
            IF Command > 0 THEN
				InternalDirection := Direction;
                IF InternalDirection = 1 OR InternalDirection = 2 OR Command = 3 THEN
                    IF Command = 1 THEN //Homing
                        InternalState := 10;
                    ELSIF Command = 2 THEN //pos relative
                        InternalState := 20;
                    ELSIF Command = 3 THEN  //pos absolute
                        //check if axis is homed
                        IF InternalHomingState THEN
                            InternalPosition := Position;
                            InternalState := 30;
                        ELSE
                            InternalState := 1000;
                        END_IF;
                    ELSIF Command = 4 THEN //move velocity
                        //check for valid direction
                        InternalState := 40;
                    ELSE //invalid command
                        InternalState := 1000;
                        tempErrorID := 2;
                        Busy := FALSE;
                        Error := TRUE;
                    END_IF;
                ELSE //invalid direction
                    InternalState := 1000;
                    tempErrorID := 2;
                END_IF;
            ELSE //no command selected
                InternalState := 1000;
                tempErrorID := 1;
                Busy := FALSE;
                Error := TRUE;
            END_IF;
        ELSIF Jog THEN //jog axis
            //Check for valid direction
            IF Direction = 1 OR Direction = 2 THEN
                InternalDirection := Direction;
                InternalState := 50;
            ELSE
                InternalState := 1000;
            END_IF;
        END_IF;

    //-------------------- Homing ---------------------------
    ELSIF InternalState = 10 THEN
		InternalHomingState := FALSE;
        IF NOT HwInRefSw THEN
            IF InternalDirection = 1 THEN
                tempMotorFwd := TRUE;
            ELSIF InternalDirection = 2 THEN
                tempMotorBackw := TRUE;
            END_IF;
        ELSE
            tempCounterControlByte := USInt#1;
			InternalHomingState := TRUE;
            InternalState := 19;
        END_IF;
    ELSIF InternalState = 19 THEN
        IF NOT Execute THEN
            InternalState := 0;
        END_IF;
    //-------------------- Pos relative ---------------------
    ELSIF InternalState = 20 THEN
        IF InternalDirection = 1 THEN
            InternalPosition := HwInCounterVal + Position;
        ELSIF InternalDirection = 2 THEN
            InternalPosition := HwInCounterVal - Position;
        END_IF;
        IF Position = DInt#0 THEN
            InternalState := 29;
		ELSE
            InternalState := 21;
        END_IF;
    ELSIF InternalState = 21 THEN
        IF InternalPosition <= HwInCounterVal AND InternalDirection = 1 OR
		InternalPosition >= HwInCounterVal AND InternalDirection = 2 OR
		InternalDirection = 1 AND hwInEndSwFwd OR
		InternalDirection = 2 AND HwInEndSwBackw
	THEN
            InternalState := 29;
        ELSE
            IF InternalDirection = 1 THEN
                tempMotorFwd := TRUE;
            ELSIF InternalDirection = 2 THEN
                tempMotorBackw := TRUE;
            END_IF;
        END_IF;
    ELSIF InternalState = 29 THEN
        IF NOT Execute THEN
            InternalState := 0;
        END_IF;
    //-------------------- Pos absolute ---------------------
    ELSIF InternalState = 30 THEN
        IF InternalPosition >= HwInCounterVal - PosWindow AND InternalPosition <= HwInCounterVal + PosWindow THEN
            InternalState := 39;
        ELSE
            IF InternalPosition > HwInCounterVal THEN
                tempMotorFwd := TRUE;
            ELSE
                tempMotorBackw := TRUE;
            END_IF;
        END_IF;
    ELSIF InternalState = 39 THEN
        IF NOT Execute THEN
            InternalState := 0;
        END_IF;
        
    //-------------------- Move Velocity --------------------
    ELSIF InternalState = 40 THEN
        //move if the target end switch is triggered
        IF NOT HwInEndSwFwd AND InternalDirection = 1 OR NOT HwInEndSwBackw AND InternalDirection = 2 AND Execute THEN
            tempMotorFwd    := InternalDirection = 1;
            tempMotorBackw  := InternalDirection = 2;
        ELSE
            InternalState := 49;
        END_IF;
    
    ELSIF InternalState = 49 THEN
        IF NOT Execute THEN
            InternalState := 0;
        END_IF;

    //-------------------- Jog ------------------------------
    ELSIF InternalState = 50 THEN
        IF NOT Jog THEN
            InternalState := 0;
        ELSE
            IF InternalDirection = 1 THEN
                IF NOT HwInEndSwFwd THEN
                    tempMotorFwd := TRUE;
                ELSE
                    InternalState := 1000;
                    tempErrorID := 3;
                END_IF;
            ELSIF InternalDirection = 2 THEN
                IF NOT HwInEndSwBackw THEN
                    tempMotorBackw := TRUE;
                ELSE
                    InternalState := 1000;
                    tempErrorID := 4;
                END_IF;
            END_IF;
        END_IF;

    //-------------------- Error ----------------------------
    ELSIF InternalState = 1000 THEN
        //jump to idle, if command has been removed
        IF NOT Execute AND NOT Jog THEN
            InternalState := 0;
        END_IF;
    END_IF;

    //Status output
    PosAxis := tempErrorID;
    IF InternalState >= 10 AND InternalState < 20 THEN
        ActiveCommand := 1;
    ELSIF InternalState >= 20 AND InternalState < 30 THEN
        ActiveCommand := 2;
    ELSIF InternalState >= 30 AND InternalState < 40 THEN
        ActiveCommand := 3;
    ELSIF InternalState >= 40 AND InternalState < 50 THEN
        ActiveCommand := 4;
    ELSIF InternalState >= 50 AND InternalState < 60 THEN
        ActiveCommand := 5;
    ELSE 
        ActiveCommand := 0;
    END_IF;

    //busy for all states where work is done
    Busy := InternalState >= 10 AND InternalState < 1000 AND InternalState <> 19 AND InternalState <> 29 AND InternalState <> 39 AND InternalState <> 49 AND InternalState <> 59;
    //error is set for error step
    Error := InternalState = 1000;
    //done is always the last step for a command
    Done := InternalState = 19 OR InternalState = 29 OR InternalState = 39 OR InternalState = 49 OR InternalState = 59;
    //write hardware outputs
    HwOutMotorFwd := tempMotorFwd;
    HwOutMotorBackw := tempMotorBackw;
    HwOutCounterControlVal  := tempCounterControlBits;
    HwOutCounterControlByte := tempCounterControlByte;

    //edge detection
    InternalEdgeDetect := Execute;

END_FUNCTION

TYPE
    PusherInternalVal : STRUCT
        CountEnable     : Bool;
        CountValue      : Int;
        InternalState   : Int;
    END_STRUCT;
END_TYPE

FUNCTION Pusher
    VAR_INPUT
        ExecutePush             : Bool; //start push on positive signal edge
        CompressorIsRunning     : Bool; //feedback for a running compressor
        PushTime_ms             : Int; //configurable push time
        CompressorTimeout_ms    : Int; //timeout for error generation
        CallCycleTime_ms        : Int; //no timers available yet, function call has to be cyclic, cycle time cannot be read out, needs to be an input
    END_VAR
    VAR_OUTPUT
        Busy            : Bool; //true: the function is active in doing its work
        Error           : Bool; //true: while working, there was an error. Nothing is active any more
        Done            : Bool; //true: work command is done, active for one cycle or while execute is still set
        HwOutPusher     : Bool; //hardware output to hardware adress
        ReqEnCompressor : Bool; //request for turning on the compressor, has to be connected to the compressor module input
    END_VAR
    VAR_IN_OUT
        InternalValues : PusherInternalVal;
    END_VAR
    VAR_TEMP
        tmpPusher : Bool;
        tmpCompressor : Bool;
    END_VAR
        
        IF InternalValues.InternalState = 0 THEN //idle
            IF ExecutePush THEN
                IF PushTime_ms > 0 AND CompressorTimeout_ms > 0 AND CallcycleTime_ms > 0 THEN
                    tmpCompressor := TRUE;
                    InternalValues.InternalState := 10;
                ELSE
                    InternalValues.InternalState := 40;
                END_IF;
            END_IF;

        ELSIF InternalValues.InternalState = 10 THEN //wait for compressor running
            InternalValues.CountEnable := TRUE;
            tmpCompressor := TRUE;
            IF CompressorIsRunning THEN
                tmpPusher := TRUE;
                InternalValues.CountEnable := FALSE;
                InternalValues.InternalState := 20;
            ELSIF InternalValues.CountValue * CallCycleTime_ms >= CompressorTimeout_ms THEN
                InternalValues.CountEnable := FALSE;
                InternalValues.InternalState := 40; //error, timeout of Compressor
            END_IF;
        
        ELSIF InternalValues.InternalState = 20 THEN //execute push
            tmpCompressor := TRUE;
            tmpPusher := TRUE;
            InternalValues.CountEnable := TRUE;
            IF InternalValues.CountValue * CallCycleTime_ms >= PushTime_ms THEN //push time is matched/exeeded
                tmpPusher := FALSE;
                tmpCompressor := FALSE;
                InternalValues.CountEnable := FALSE; //clear counter
                InternalValues.InternalState := 30; //done
            ELSIF NOT CompressorIsRunning THEN //error, the compressor has been deactivated while pushing
                tmpPusher := FALSE;
                tmpCompressor := FALSE;
                InternalValues.CountEnable := FALSE; //clear counter
                InternalValues.InternalState := 40; //error
            END_IF;

        ELSIF InternalValues.InternalState = 30 THEN //push done, wait for disable
            IF NOT ExecutePush THEN
                InternalValues.InternalState := 0; //back to idle if execute has been cleared
            END_IF;

        ELSIF InternalValues.InternalState = 40 THEN //error
            IF NOT ExecutePush THEN
                InternalValues.InternalState := 0;
            END_IF;

        END_IF;

        //start/stop counter
        IF InternalValues.CountEnable THEN
            InternalValues.CountValue := InternalValues.CountValue + 1;
        ELSE
            InternalValues.CountValue := 0; //reset counter
        END_IF;

        //set status bits
        IF InternalValues.InternalState = 0 THEN
            Busy    := FALSE;
            Error   := FALSE;
            Done    := FALSE;
        ELSIF InternalValues.InternalState = 10 THEN
            Busy    := TRUE;
            Error   := FALSE;
            Done    := FALSE;
        ELSIF InternalValues.InternalState = 20 THEN
            Busy    := TRUE;
            Error   := FALSE;
            Done    := FALSE;
        ELSIF InternalValues.InternalState = 30 THEN
            Busy    := FALSE;
            Error   := FALSE;
            Done    := TRUE;
        ELSIF InternalValues.InternalState = 40 THEN
            Busy    := FALSE;
            Error   := TRUE;
            Done    := FALSE;
        ELSE
            Busy    := FALSE;
            Error   := FALSE;
            Done    := FALSE;
        END_IF;

        HwOutPusher := tmpPusher;
        ReqEnCompressor := tmpCompressor;

END_FUNCTION

TYPE
    CompressorInternalVal : STRUCT
        CountEnable     : Bool;
        CountValue      : Int;
        InternalState   : Int;
    END_STRUCT;
END_TYPE

FUNCTION Compressor
    VAR_INPUT
        ReqEnable     : Bool;
        StartDelay_ms : Int;
        CycleTime_ms  : Int;
    END_VAR
    VAR_OUTPUT
        IsRunning       : Bool;
        Error           : Bool;
        HwOutCompressor : Bool;
    END_VAR
    VAR_IN_OUT
        InternalValues : CompressorInternalVal;
    END_VAR
    VAR_TEMP
        tmpSwitchOn  : Bool;
        tmpIsRunning : Bool;
        tmpError     : Bool;
    END_VAR

    IF InternalValues.InternalState = 0 THEN
        IF ReqEnable THEN
            IF CycleTime_ms > 0 THEN
                InternalValues.InternalState := 1;
                InternalValues.CountEnable := TRUE;
                tmpSwitchOn := TRUE;
            ELSE
                InternalValues.InternalState := 10;
                tmpError := TRUE;
            END_IF;
        END_IF;
    ELSIF InternalValues.InternalState = 1 THEN
        IF Internalvalues.CountValue >= StartDelay_ms / CycleTime_ms THEN
            tmpSwitchOn := TRUE;
            InternalValues.CountEnable := FALSE;
            InternalValues.InternalState := 2;
            tmpIsRunning := TRUE;
        END_IF;
    ELSIF InternalValues.InternalState = 2 THEN
        tmpSwitchOn := TRUE;
        tmpIsRunning := TRUE;
        IF NOT ReqEnable THEN
            InternalValues.InternalState := 0;
            tmpSwitchOn := FALSE;
            tmpIsRunning := FALSE;
        END_IF;
    ELSIF InternalValues.InternalState = 10 THEN
        tmpError := TRUE;
        IF NOT ReqEnable THEN
            InternalValues.InternalState := 0;
            tmpError := FALSE;
        END_IF;
    END_IF;
    IF InternalValues.CountEnable THEN
        InternalValues.CountValue := InternalValues.CountValue + 1;
    ELSE
        InternalValues.CountValue := 0;
    END_IF;
    IsRunning := tmpIsRunning;
    Error := tmpError;
    HwOutCompressor := tmpSwitchOn;
END_FUNCTION

END_NAMESPACE